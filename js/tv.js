$(document).on("navSelected", "a.tv.selected", function() {
  //Chart Config
  var $market = 1,
    $mktRank = $(".mkt-rank"),
    $initYear = "2018",
    $endYear = "2022",
    $pallet = "Soft Pastel",
    $legendShow = true,
    $tooltipShow = true,
    $exportShow = false,
    $labelOverlap = "shift",
    $adaptLayout = 300,
    $tooltipType = "currency",
    $tooltipPrec = 0,
    $tooltipCurr = "USD",
    $animation = { enabled: "easeOutCubic", duration: 500 },
    $series = [
      {
        argumentField: "name",
        valueField: "total",
        border: {
          visible: "true",
          width: 0.5
        },
        label: {
          visible: true,
          displayMode: "stagger",
          staggeringSpacing: 20,
          position: "columns",
          font: {
            size: 12
          },
          connector: {
            visible: true,
            width: 1
          },
          border: {
            visible: true,
            color: "rgba(0, 0, 0, 0.25)",
            width: 1
          },
          format: "fixedPoint",
          percentPrecision: 1,
          customizeText: function(pnt) {
            var series = pnt.point.series.getAllPoints();
            series.sort(
              (a, b) => Number(b.percent * 100) - Number(a.percent * 100)
            );

            var pntIndex = series.findIndex(x => x.argument == pnt.argument);

            // only display the top 8
            // counting on the sort order to max -> min
            if (pntIndex < 5) {
              return "<b>" + pnt.percentText + "</b><br>" + pnt.argumentText;
            }

            return "";
          }
        },
        minSegmentSize: 5,
        onLegendClick: function(info) {
          var pieChart = info.component;
          var argument = info.target;
          pieChart
            .getAllSeries()[0]
            .getPointsByArg(argument)[0]
            .showTooltip();
        }
      }
    ];

  loadTvContent();

  function loadTvContent() {
    $(".select-item.market").on("change.tv", function() {
      if ($(".tv").hasClass("selected")) {
        loadTvContent();
      } else {
        $(".select-item.market").off("change.tv");
      }
    });

    $market = getSelectedMarket().val();

    if ($market != "") {
      var $token = authToken(),
        $mktSelected = getSelectedMarket().text();

      $mktRank.text(getSelectedMarket().data("rank"));

      //reresh session
      $token ? setAuthToken($token) : location.reload();
      mktTitle($mktSelected);
      function mktTitle() {
        if ($(".tv .mkt-title").length < 2) {
          window.requestAnimationFrame(mktTitle);
        } else {
          $(".tv .mkt-title, .navbar-brand .mkt-title").text($mktSelected);
        }
      }

      showLoadPanel($("#loadPanel"), $(".cd-main"), true);

      $(".select-item.subvert").removeClass("enable");
      $(".select-item.market.main-market, .navbar-brand").addClass("enable");

      //Get TV Market Comp
      var tvComp = [],
        tvCompUrl =
          "https://advantage.bia.com/api/MediaAdView/GetOTARevenues/" + $market;
      $.ajax({
        url: tvCompUrl,
        async: true,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        headers: { "x-api-version": "2.0" },
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Bearer " + $token);
        },
        success: function(tvCompData) {
          if (tvCompData) {
            var tvCompTotal = tvCompData.tvOTATotal,
              $tvCompTotal = $(".tvComp-total");
            $tvCompTotal.text(
              "$" +
                tvCompTotal
                  .toFixed(0)
                  .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +
                " (000)s"
            );
            $.each(tvCompData.topTvOwners, function(index, value) {
              tvComp.push({
                name: value.name,
                total: value.revenue
              });
            });

            $("#tvComp").dxPieChart({
              palette: $pallet,
              dataSource: tvComp,
              legend: {
                visible: true,
                rowCount: 3,
                verticalAlignment: "bottom",
                horizontalAlignment: "center",
                itemTextPosition: "right",
                customizeText: function() {
                  var pieChart = $("#tvComp").dxPieChart("instance"),
                    percentValue = (
                      pieChart.getAllSeries()[0].getAllPoints()[this.pointIndex]
                        .percent * 100
                    ).toFixed(1);
                  return this.pointName + " (" + percentValue + "%)";
                }
              },
              tooltip: {
                enabled: $tooltipShow,
                format: {
                  type: $tooltipType,
                  precision: $tooltipPrec,
                  currency: $tooltipCurr
                },
                customizeTooltip: function(args) {
                  return {
                    html:
                      "<div><h5>" +
                      args.argumentText +
                      "</h5></div>" +
                      "<div><b>Amount: </b>" +
                      args.valueText +
                      "</div>" +
                      "<div><b>Percentage: </b>" +
                      (args.percent * 100).toFixed(1) +
                      " %</div>"
                  };
                }
              },
              export: {
                enabled: $exportShow
              },
              resolveLabelOverlapping: $labelOverlap,
              adaptiveLayout: {
                width: $adaptLayout
              },
              animation: $animation,
              series: $series
            });

            setupPrint(
              $("#tvCompPrint"),
              $("#tvComp"),
              $("#tvCompPrintTooltip")
            );
            setupExport(
              $("#tvCompExport"),
              $("#tvComp"),
              $("#tvCompExportTooltip")
            );
          }
        },
        error: function(err) {
          console.log("Error: Cannot Reach API");
        }
      });

      //Update Year Selection
      $(".tvComp .button.prev").unbind();
      $(".tvComp .button.prev").on("click", function() {
        $(".tvComp .button.next").removeClass("disabled");
        var $year = $(".tvComp .year").text(),
          $yearNum = parseInt($year);
        if ($yearNum != $initYear) {
          $(".tvComp .year a").text($yearNum - 1);
        }
        if ($yearNum == $initYear + 1) {
          $(".tvComp .button.prev").addClass("disabled");
        }
        var $tvCompYr = $(".tvComp .year").text(),
          tvComp = [],
          tvCompUrl =
            "https://advantage.bia.com/api/MediaAdView/GetOTARevenues/" +
            $market;
        $.ajax({
          url: tvCompUrl,
          async: true,
          type: "GET",
          dataType: "json",
          contentType: "application/json",
          headers: { "x-api-version": "2.0" },
          beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + $token);
          },
          success: function(tvCompData) {
            if (tvCompData) {
              console.log(tvCompData);
              var tvCompTotal = tvCompData.tvOTATotal,
                $tvCompTotal = $(".tvComp-total");
              $tvCompTotal.text(
                "$" +
                  tvCompTotal
                    .toFixed(0)
                    .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +
                  " (000)s"
              );
              $.each(tvCompData.topTvOwners, function(index, value) {
                tvComp.push({
                  name: value.name,
                  total: value.revenue
                });
              });
              $("#tvComp").dxPieChart("option", "dataSource", tvComp);
            }
          },
          error: function(err) {
            console.log("Error: Cannot Reach API");
          }
        });
      });

      $(".tvComp .button.next").unbind();
      $(".tvComp .button.next").on("click", function() {
        $(".tvComp .button.prev").removeClass("disabled");
        var $year = $(".tvComp .year").text(),
          $yearNum = parseInt($year);
        if ($yearNum != $endYear) {
          $(".tvComp .year a").text($yearNum + 1);
        }
        if ($yearNum == $endYear - 1) {
          $(".tvComp .button.next").addClass("disabled");
        }
        var $tvCompYr = $(".tvComp .year").text(),
          tvComp = [],
          tvCompUrl =
            "https://advantage.bia.com/api/MediaAdView/GetOTARevenues/" +
            $market;
        $.ajax({
          url: tvCompUrl,
          async: true,
          type: "GET",
          dataType: "json",
          contentType: "application/json",
          headers: { "x-api-version": "2.0" },
          beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + $token);
          },
          success: function(tvCompData) {
            if (tvCompData) {
              console.log(tvCompData);
              var tvCompTotal = tvCompData.tvOTATotal,
                $tvCompTotal = $(".tvComp-total");
              $tvCompTotal.text(
                "$" +
                  tvCompTotal
                    .toFixed(0)
                    .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +
                  " (000)s"
              );
              $.each(tvCompData.topTvOwners, function(index, value) {
                tvComp.push({
                  name: value.name,
                  total: value.revenue
                });
              });
              $("#tvComp").dxPieChart("option", "dataSource", tvComp);
            }
          },
          error: function(err) {
            console.log("Error: Cannot Reach API");
          }
        });
      });

      //Get TV Market Revenues
      var tvRevenue = [],
        tvRevenueUrl =
          "https://advantage.bia.com/api/MediaAdView/GetOTARevenues/" + $market;
      $.ajax({
        url: tvRevenueUrl,
        async: true,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        headers: { "x-api-version": "2.0" },
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Bearer " + $token);
        },
        success: function(tvRevenueData) {
          if (tvRevenueData) {
            $.each(tvRevenueData.tvEstimatedRevenues, function(index, value) {
              tvRevenue.push({
                name: String(value.year),
                ota: value.ota,
                online: value.online,
                retrans:
                  value.retransmissions > 0 ? value.retransmissions : null
              });
            });

            $("#tvRevenue").dxChart({
              dataSource: tvRevenue,
              commonAxisSettings: {
                maxValueMargin: 0.1
              },
              commonSeriesSettings: {
                argumentField: "name",
                type: "bar",
                hoverMode: "allArgumentPoints",
                selectionMode: "allArgumentPoints",
                label: {
                  visible: true,
                  format: {
                    type: "fixedPoint",
                    precision: 0
                  }
                }
              },
              series: [
                {
                  valueField: "ota",
                  name: "Over-The-Air",
                  ignoreEmptyPoints: true
                },
                {
                  valueField: "online",
                  name: "Online",
                  ignoreEmptyPoints: true
                },
                {
                  valueField: "retrans",
                  name: "Retransmission",
                  ignoreEmptyPoints: true
                }
              ],
              title: "",
              legend: {
                verticalAlignment: "top",
                horizontalAlignment: "center"
              },
              valueAxis: {
                title: {
                  text: "millions"
                },
                label: {
                  customizeText: function() {
                    return this.value / 1000;
                  }
                },
                position: "left"
              },
              export: {
                enabled: $exportShow
              },
              onPointClick: function(e) {
                e.target.select();
              }
            });

            setupPrint(
              $("#tvRevenuePrint"),
              $("#tvRevenue"),
              $("#tvRevenuePrintTooltip")
            );
            setupExport(
              $("#tvRevenueExport"),
              $("#tvRevenue"),
              $("#tvRevenueExportTooltip")
            );

            hideLoadPanel($("#loadPanel"));
          }
        },
        error: function(err) {
          console.log("Error: Cannot Reach API");
        }
      });

      setTimeout(function() {
        var PDFUrl =
          "https://advantage.bia.com/api/MediaAdView/GetTvInvestingInPDF/" +
          $market;

        $("#pdfViewer").attr(
          "src",
          "pdfview/viewer.html?file=" + PDFUrl + "#page=2"
        );
      }, 800);

      //TODO
      // we need to fix this so that it handles the authentication headers
      // not enough time to worry about it at the moment

      /*
                  PDFJS.getDocument(tvPDFUrl).then(function (pdf) {
                      var pageNumber = 2;
          
                      pdf.getPage(pageNumber).then(function (page) {
                          var viewPort = page.getViewPort(1);
                          var canvas = document.getElementById("viewer");
                          var context = canvas.getContext("2d");
                          canvas.height = viewPort.height;
                          canvas.width = viewPort.width;
          
                          page.render({
                              canvasContext: context,
                              viewport: viewport
                          });
                      });
                  });
          */
      /*		  
              $.ajax({
                  url: tvPDFUrl,
                  async: true,
                  type: "GET",
                  dataType: "text",
                  processData: false,
                  headers: { "x-api-version": "2.0" },
                  beforeSend: function(xhr) {
                  xhr.setRequestHeader("Authorization", "Bearer " + $token);
                  },
                  success: function(tvPDFData) {
                  if (tvPDFData) {
                      var fr = new FileReader();
                      fr.onload = function () {
                          var arraybuffer = this.result;
                          var tvData = new Uint8Array(arraybuffer);
          
                          PDFJS.getDocument({
                              data: tvData
                          }).then(function (pdf) {
                              var pageNumber = 2;
          
                              pdf.getPage(pageNumber).then(function (page) {
                                  var viewPort = page.getViewPort(1);
                                  var canvas = document.getElementById("pdfCanvas");
                                  var context = canvas.getContext("2d");
                                  canvas.height = viewPort.height;
                                  canvas.width = viewPort.width;
          
                                  page.render({
                                      canvasContext: context,
                                      viewport: viewport
                                  });
                              });
                          });
                      }
          
                      fr.readAsArrayBuffer(new Blob([tvPDFData], {type: 'image/png'}));
                  }
                  },
                  error: function(err) {
                  console.log("Error: Cannot Reach API");
                  }
              });		
          */
    }
  }
});
