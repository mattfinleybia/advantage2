$(document).on("navSelected", "a.ad-forecast.selected", function() {
  showLoadPanel($("#loadPanel"), $(".cd-main"), true);

  $(".select-item.subvert").removeClass("enable");
  $(".select-item.market.main-market, .navbar-brand").removeClass("enable");

  getJson(getBaseHref() + "api/Dashboard/GetPublishedSlug/usadforecasts")
    .done(function(data) {
      // add the returned data to the div
      $("#usadforecastcontent").html(data.content.rendered);

      // set the base href for all img tags
      $("#usadforecastcontent img").attr("src", function(index, src) {
        return "https://dashboard.biakelsey.com/" + src;
      });

      $("#usadforecastcontent a[href^='/']").attr("href", function(index, src) {
        //if (src.toString().toLowerCase().indexOf('https://') >= 0)
        //    return src;

        return "https://dashboard.biakelsey.com/" + src;
      });

      // remove all class elements
      //$('#usadforecastcontent').find("*").prop("class", "");
      var usadForecastContent = $("#usadforecastcontent");
      usadForecastContent
        .find(".vc_row")
        .addClass("row")
        .removeClass("vc_row");
      usadForecastContent
        .find(".vc_col-sm-12")
        .addClass("col-lg-12")
        .removeClass("vc_col-sm-12");
      usadForecastContent
        .find(".vc_col-sm-8")
        .addClass("col-lg-8")
        .removeClass("vc_col-sm-8");
      usadForecastContent
        .find(".vc_col-sm-4")
        .addClass("col-lg-4")
        .removeClass("vc_col-sm-4");
      usadForecastContent
        .find(".vc_col-sm-3")
        .addClass("col-lg-3")
        .removeClass("vc_col-sm-3");

      hideLoadPanel($("#loadPanel"));
    })
    .fail(function(err) {
      hideLoadPanel($("#loadPanel"));
    });
});
