$(function() {
  "use strict";
  var isAnimating = false,
    firstLoad = false,
    $market = $(".main-market .filters-market"),
    $homeMarket = $(".home-market"),
    $quickMarket = $(".quick-market"),
    $marketTooltip = $(".home-market .tooltip"),
    $qkmarketTooltip = $(".quick-market .tooltip"),
    $subvertical = $(".main-subvert .filters-subvert"),
    dashboard = $("#nav"),
    mainContent = $(".cd-main");

  //select a disabled option
  dashboard.on("click", "li", function(e) {
    if (
      $(this)
        .children("a")
        .hasClass("disabled")
    ) {
      $marketTooltip.addClass("invalid");
      $marketTooltip.prev().addClass("invalid");
      $homeMarket.unbind();
      $homeMarket.on("click", function() {
        $marketTooltip.removeClass("invalid");
        $marketTooltip.prev().removeClass("invalid");
      });
    }
  });

  dashboard.on("marketChanged", function(e, obj) {
    marketSelect(obj.selectedMarket);
  });

  dashboard.on("qmarketChanged", function(e, obj) {
    qverticalSelect(obj.selectedMarket, obj.vertical);
  });

  dashboard.on("subverticalChanged", function(e, obj) {
    subvertSelect(obj.selectedMarket, obj.subVertical, obj.vertical);
  });

  function marketSelect(mktSelected) {
    var currentMarket = mktSelected;

    if (!currentMarket) {
      currentMarket = getSelectedMarket().val();
    }

    var sectionTarget = "summary",
      currentTarget = dashboard.find("a.summary");

    currentTarget.addClass("selected");

    dashboard
      .find("a")
      .not(currentTarget)
      .removeClass("selected");

    $market.dropdown("set selected", currentMarket);

    activeItem(currentTarget);

    triggerAnimation(currentTarget, sectionTarget, true);
  }

  function qverticalSelect(mktSelected, verticalSelected) {
    //select a quick vertical from home
    var currentMarket = mktSelected,
      currentSubvert = verticalSelected,
      sectionTarget = "subvertical",
      currentTarget = dashboard.find("a.vertical");
    if (currentMarket.length) {
      currentTarget.addClass("selected");
      dashboard
        .find("a")
        .not(currentTarget)
        .removeClass("selected");
      activeItem(currentTarget);
      $(
        ".select-item.market.main-market, .navbar-brand, .select-item.subvert"
      ).addClass("enable");

      $market.dropdown("set selected", currentMarket);
      $subvertical.dropdown("set selected", currentSubvert);

      triggerAnimation(currentTarget, sectionTarget, true);
    } else {
      $qkmarketTooltip.addClass("invalid");
      $qkmarketTooltip.prev().addClass("invalid");
      $(".quick-subvert .filters-subvert").dropdown("clear");
      $quickMarket.unbind();
      $quickMarket.on("click", function() {
        $qkmarketTooltip.removeClass("invalid");
        $qkmarketTooltip.prev().removeClass("invalid");
      });
    }
  }

  //select a vertical
  dashboard.on("click", "a", function(e) {
    var currentMarket = $market.children("option:selected").val(),
      target = $(this),
      sectionTarget = target.data("menu");

    if (target.hasClass("contact-us")) {
      e.preventDefault();
      return;
    }

    if (sectionTarget == "index") {
      // clean up our event handlers
      $(".select-item.market").off("change.summary");
      $(".select-item.market").off("change.vertical");

      $subvertical.dropdown("clear");
      $market.dropdown("clear");

      //        marketSelect();
      //        qverticalSelect();
    }

    verticalSelect(target, sectionTarget, currentMarket);
  });

  function verticalSelect($target, $sectionTarget, $currentMarket) {
    $(".filters-subvert .menu").addClass("is-vertical");

    if (typeof $sectionTarget != "undefined" && !$target.hasClass("selected")) {
      $(".filters-subvert").dropdown("clear");
      $(".filters-subvert .menu").empty();

      //RH
      // not sure what this is going to break
      // but this was causing event handlers to be triggered
      // multiple times. Not sure why we didn't just trigger an event
      // to do the same thing if needed instead of firing all the change
      // handlers.

      //setTimeout(function() {
      //$market.dropdown("restore defaults");
      //$market.dropdown("set selected ", $currentMarket);
      //}, 100);

      $target.addClass("selected");
      dashboard
        .find("a")
        .not($target)
        .removeClass("selected");
      $(".tooltip").addClass("show");
      setTimeout(function() {
        $(".sidebar").removeClass("sidebar-show");
        $(".cd-section").removeClass("sidebar-shift");
        $("#cd-main-nav ul").removeClass("nav-shift");
      }, 200);
      activeItem($target);
      //if user has selected a section different from the one alredy visible - load the new content
      triggerAnimation($target, $sectionTarget, true);
    }
    firstLoad = true;
  }

  //select a subvertical
  function subvertSelect(mktSelected, subVertId, vertId) {
    var currentMarket = mktSelected,
      $sectionTarget = "subvertical",
      summaryTarget = dashboard.find("a.vertical");

    summaryTarget.addClass("selected");

    dashboard
      .find("a")
      .not(summaryTarget)
      .removeClass("selected");

    activeItem(summaryTarget);

    $(".filters-subvert .menu").removeClass("is-vertical");
    if (subVertId != "" && !isAnimating) {
      //setTimeout(function() {
      //  $market.dropdown("restore defaults");
      //  $market.dropdown("set selected", $currentMarket);
      //}, 100);
      triggerAnimation(summaryTarget, $sectionTarget, true);
    }
  }

  //updated selected nav item
  function activeItem(currentItem) {
    dashboard.find("li").removeClass("active");
    if (currentItem.attr("data-menu") === "vertical") {
      currentItem
        .parents()
        .eq(3)
        .prev()
        .addClass("active");
    } else {
      currentItem.parent().addClass("active");
    }

    // if this is the radio nav then we need to remove markets
    // that do not have a radio market
    if (currentItem.attr("data-menu") != "radio") {
      $(".filters-market .item").removeClass("disabled");
    }
  }

  //start page transition
  function triggerAnimation(eventTarget, newSection, bool) {
    isAnimating = true;
    newSection = newSection == "" ? "index" : newSection;
    //load new content
    loadNewContent(eventTarget, newSection, bool);
  }

  function loadNewContent(eventTarget, newSection, bool) {
    //create a new section element and insert it into the DOM
    var section = $(
      '<section class="cd-section overflow-hidden ' +
        newSection +
        '"></section>'
    ).appendTo(mainContent);
    //load the new content from the proper html file
    section.load(newSection + ".html .cd-section > *", function() {
      //finish up the animation and then make the new section visible
      //add the .visible class to the new section element -> it will cover the old one
      section
        .prev(".visible")
        .removeClass("visible")
        .hide()
        .end()
        .addClass("visible")
        .on(
          "webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",
          function() {
            resetAfterAnimation(eventTarget, section);
          }
        );

      //if browser doesn't support transition
      if ($(".no-csstransitions").length > 0) {
        resetAfterAnimation(eventTarget, section);
      }
      //      var url = newSection + ".html";
      //      if (url != window.location && bool) {
      //add the new page to the window.history
      //if the new page was triggered by a 'popstate' event, don't add it
      //        window.history.pushState({ path: url }, "", url);
      //      }
    });
  }

  function resetAfterAnimation(eventTarget, newSection) {
    //once the new section is added, remove the old section and make the new one scrollable
    newSection
      .removeClass("overflow-hidden")
      .prev(".cd-section")
      .remove();

    if (isAnimating == true) eventTarget.trigger("navSelected");

    isAnimating = false;
  }
});
