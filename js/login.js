$(function() {
  var formModal = $(".login-user-modal"),
    formLogin = formModal.find("#login-login"),
    formChangePassword = formModal.find("#login-change-password"),
    formForgotPassword = formModal.find("#login-reset-password"),
    formNewUser = formModal.find("#login-new-user"),
    formManageUser = formModal.find("#login-manage-user"),
    formNewCustomer = formModal.find("#login-new-customer"),
    formManageCustomer = formModal.find("#login-manage-customer"),
    formModalTab = $(".login-switcher"),
    tabNewUser = formModalTab
      .children("li")
      .eq(0)
      .children("a"),
    tabManageUser = formModalTab
      .children("li")
      .eq(1)
      .children("a"),
    tabNewCustomer = formModalTab
      .children("li")
      .eq(2)
      .children("a"),
    tabManageCustomer = formModalTab
      .children("li")
      .eq(3)
      .children("a"),
    loginLink = $(".login-signin"),
    accountLink = $(".account"),
    forgotPasswordLink = formLogin.find(".login-form-bottom-message a"),
    backToLoginLink = formForgotPassword.find(".login-form-bottom-message a"),
    users = $("#user-id");

  //check if already logged in
  if (authToken() == null) {
    login_selected();
  } else {
    loginLink
      .html(loginLink.html().replace("in", "out"))
      .removeClass("logged-out");
  }

  //log out
  loginLink.on("click", function(event) {
    if (!loginLink.hasClass("logged-out")) {
      lscache.remove("token");
      lscache.flush();
      location.reload();
    }
  });

  //close modal
  formModal.on("click", function(event) {
    if (!formLogin.hasClass("is-selected")) {
      if (
        $(event.target).is(formModal) ||
        $(event.target).is(".login-close-form")
      ) {
        formModal.removeClass("is-visible");
      }
    }
  });

  //close modal when clicking the esc keyboard button
  $(document).keyup(function(event) {
    if (!formLogin.hasClass("is-selected")) {
      if (event.which == "27") {
        formModal.removeClass("is-visible");
      }
    }
  });

  //switch from a tab to another
  formModalTab.on("click", function(event) {
    event.preventDefault();
    if ($(event.target).is(tabNewUser)) {
      newuser_selected();
    } else if ($(event.target).is(tabManageUser)) {
      manageuser_selected();
    } else if ($(event.target).is(tabNewCustomer)) {
      newcustomer_selected();
    } else {
      managecustomer_selected();
    }
  });

  //hide or show password
  $(".hide-password").on("click", function() {
    var togglePass = $(this),
      passwordField = togglePass.prev("input");
    "password" == passwordField.attr("type")
      ? passwordField.attr("type", "text")
      : passwordField.attr("type", "password");
    "Hide" == togglePass.text()
      ? togglePass.text("Show")
      : togglePass.text("Hide");
    //focus and move cursor to the end of input field
    passwordField.putCursorAtEnd();
  });

  //show change-password form
  accountLink.on("click", function(event) {
    event.preventDefault();
    IsInRole("Administrator") ? newuser_selected() : account_selected();
  });

  //show forgot-password form
  forgotPasswordLink.on("click", function(event) {
    event.preventDefault();
    forgot_password_selected();
  });

  //back to login from the forgot-password form
  backToLoginLink.on("click", function(event) {
    event.preventDefault();
    login_selected();
  });

  function login_selected() {
    formModal.addClass("is-visible");
    formLogin.addClass("is-selected");
    formForgotPassword.removeClass("is-selected");
  }

  function account_selected() {
    formModal.addClass("is-visible");
    formLogin.removeClass("is-selected");
    formChangePassword.addClass("is-selected");
  }

  function newuser_selected() {
    $(".login-user-modal-container").addClass("new");
    formModal.addClass("is-visible");
    formLogin.removeClass("is-selected");
    formNewUser.addClass("is-selected");
    formManageUser.removeClass("is-selected");
    formNewCustomer.removeClass("is-selected");
    formManageCustomer.removeClass("is-selected");
    formModalTab.removeClass("disabled");
    tabNewUser.addClass("selected");
    tabManageUser.removeClass("selected");
    tabNewCustomer.removeClass("selected");
    tabManageCustomer.removeClass("selected");
    getMarkets();
    //Get Customers
    getJson("https://advantage.bia.com/api/Customers?includeMarkets=true")
      .done(function(customerdata) {
        var $target = $(
            "#new-user-customer, #update-customer, #customer, #manage-customer"
          ),
          option = $("<option />");
        $target.empty();
        $target.append(
          '<option value="">Customer</option><option value="0">No Customer</option>'
        );
        $.each(customerdata, function(index, value) {
          option
            .addClass("button")
            .html(value.name)
            .val(value.customerId);
          $target.append(option);
        });

        $target.dropdown({
          onChange: function(value, text, $selectedItem) {
            var currentCustomer = value;
            if (currentCustomer != "0") {
              //Get Customer Markets
              getJson(
                "https://advantage.bia.com/api/CustomerMarkets/" +
                  currentCustomer,
                value
              )
                .done(function(mktdata) {
                  var $target = $("#new-markets");
                  $target.empty();
                  $target.siblings(".ui.label").remove();
                  $target.append('<option value="">Markets</option>');
                  $.each(mktdata, function(index, markets) {
                    var option = $("<option />");
                    option
                      .addClass("button")
                      .html(markets.marketName)
                      .val(markets.marketNbr);
                    $target.append(option);
                  });
                  $target.dropdown();
                  mktSelectAll();
                })
                .fail(function(err) {
                  console.log("Unable to reach API");
                });
              getCustomer(customerdata, value);
            } else {
              getMarkets();
            }
          }
        });
      })
      .fail(function(err) {
        console.log("Unable to reach API");
      });

    //Get all Claims from Admin
    getJson(
      "https://advantage.bia.com/api/Users/GetUserById/6cd4c86e-5dc7-4882-915c-ad6ddec40cd8"
    )
      .done(function(admindata) {
        var $target = $("#new-access, #access");
        $target.empty();
        $target.siblings(".ui.label").remove();
        $target.append('<option value="">Access</option>');
        $.each(admindata.claims, function(index, value) {
          if (value.claimType != "Radio" && value.claimType != "Portal") {
            var option = $("<option />");
            option
              .addClass("button")
              .html(value.claimType)
              .val(value.claimType);
            $target.append(option);
          }
        });
        $target.dropdown();
        $("#new-access-all").checkbox({
          onChecked() {
            const options = $("#new-access > option")
              .toArray()
              .map(obj => obj.value);
            $("#new-access").dropdown("set exactly", options);
          },
          onUnchecked() {
            $("#new-access").dropdown("clear");
          }
        });
      })
      .fail(function(err) {
        console.log("Unable to reach API");
      });

    //Get Roles
    getJson("https://advantage.bia.com/api/Users/GetRoles")
      .done(function(roledata) {
        var $target = $("#roles, #new-roles");
        $target.empty();
        $target.siblings(".ui.label").remove();
        $.each(roledata, function(index, value) {
          if (value.name == "Administrator" || value.name == "Subscriber") {
            var option = $("<option />");
            option
              .addClass("button")
              .html(value.name)
              .val(value.name);
            $target.append(option);
          }
        });
        $target.dropdown();
        $("#new-roles").dropdown("set exactly", "Subscriber");
      })
      .fail(function(err) {
        console.log("Unable to reach API");
      });

    function getMarkets() {
      //Get Markets
      getJson("https://advantage.bia.com/api/Markets")
        .done(function(mktdata) {
          var $target = $(
            "#new-markets, #customer-markets, #manage-customer-markets"
          );
          $target.empty();
          $target.siblings(".ui.label").remove();
          $target.append('<option value="">Markets</option>');
          $.each(mktdata, function(index, markets) {
            var option = $("<option />");
            option
              .addClass("button")
              .html(markets.marketName)
              .val(markets.marketNbr);
            $target.append(option);
          });
          $target.dropdown();
          mktSelectAll();
        })
        .fail(function(err) {
          console.log("Unable to reach API");
        });
    }

    function mktSelectAll() {
      //Select All
      $("#new-markets-all").checkbox({
        onChecked() {
          const options = $("#new-markets > option")
            .toArray()
            .map(obj => obj.value);
          $("#new-markets").dropdown("set exactly", options);
        },
        onUnchecked() {
          //$("#new-markets").siblings('.ui.label').remove();
          $("#new-markets").dropdown("clear");
        }
      });
    }

    function getCustomer(customerdata, currentCustomer) {
      if ($("#login-manage-customer").hasClass("is-selected")) {
        var currentName,
          currentAddress,
          currentCity,
          currentState,
          currentZip,
          currentMarkets = [];
        $.each(customerdata, function(index, value) {
          if (value.customerId == currentCustomer) {
            currentName = value.name;
            currentAddress = value.address;
            currentCity = value.city;
            currentState = value.state;
            currentZip = value.zipcode;
            $.each(value.customerMarkets, function(index1, markets) {
              currentMarkets.push(markets.marketNbr.toString());
            });
            $("#name").val(currentName);
            $("#address").val(currentAddress);
            $("#city").val(currentCity);
            $("#state").dropdown("set selected", currentState);
            $("#zip").val(currentZip);
            $("#manage-customer-markets").dropdown(
              "set exactly",
              currentMarkets
            );
            $("#state, #manage-customer-markets")
              .dropdown()
              .removeClass("disabled");
            $("#manage-customer-markets-all")
              .checkbox()
              .removeClass("disabled");
            $(
              "#name, #address, #city, #zip, #delete-customer, #update-customer"
            ).removeClass("disabled");

            //Select All
            $("#manage-customer-markets-all").checkbox({
              onChecked() {
                const options = $("#manage-customer-markets > option")
                  .toArray()
                  .map(obj => obj.value);
                $("#manage-customer-markets").dropdown("set exactly", options);
              },
              onUnchecked() {
                //$("#customer-markets").siblings('.ui.label').remove();
                $("#manage-customer-markets").dropdown("clear");
              }
            });
          }
        });
      }
    }

    //Get States
    $.getJSON("data/states.json")
      .done(function(statedata) {
        var $target = $("#new-state, #state");
        $.each(statedata, function(index, value) {
          var option = $("<option />");
          option
            .addClass("button")
            .html(value.name)
            .val(value.abbreviation);
          $target.append(option);
        });
        $target.dropdown();
      })
      .fail(function(err) {
        console.log("Unable to reach API");
      });

    getUsers();
  }

  function manageuser_selected() {
    formManageUser.addClass("is-selected");
    formNewUser.removeClass("is-selected");
    formNewCustomer.removeClass("is-selected");
    formManageCustomer.removeClass("is-selected");
    tabNewUser.removeClass("selected");
    tabManageUser.addClass("selected");
    tabNewCustomer.removeClass("selected");
    tabManageCustomer.removeClass("selected");
    $("#user-markets").dropdown();
  }

  function newcustomer_selected() {
    formNewUser.removeClass("is-selected");
    formManageUser.removeClass("is-selected");
    formNewCustomer.addClass("is-selected");
    formManageCustomer.removeClass("is-selected");
    tabNewUser.removeClass("selected");
    tabManageUser.removeClass("selected");
    tabNewCustomer.addClass("selected");
    tabManageCustomer.removeClass("selected");

    //Select All
    $("#customer-markets-all").checkbox({
      onChecked() {
        const options = $("#customer-markets > option")
          .toArray()
          .map(obj => obj.value);
        $("#customer-markets").dropdown("set exactly", options);
      },
      onUnchecked() {
        //$("#customer-markets").siblings('.ui.label').remove();
        $("#customer-markets").dropdown("clear");
      }
    });
  }

  function managecustomer_selected() {
    formNewUser.removeClass("is-selected");
    formManageUser.removeClass("is-selected");
    formNewCustomer.removeClass("is-selected");
    formManageCustomer.addClass("is-selected");
    tabNewUser.removeClass("selected");
    tabManageUser.removeClass("selected");
    tabNewCustomer.removeClass("selected");
    tabManageCustomer.addClass("selected");
  }

  function forgot_password_selected() {
    formLogin.removeClass("is-selected");
    formChangePassword.removeClass("is-selected");
    formForgotPassword.addClass("is-selected");
  }

  //On Login Submit
  formLogin.find('input[type="submit"]').on("click", function(event) {
    event.preventDefault();
    var re = /\S+@\S+\.\S+/,
      email = formLogin.find("#signin-email"),
      password = formLogin.find("#signin-password");
    if (re.test(email.val()) && password.val()) {
      //Get Token
      var authUrl = "https://advantage.bia.com/api/Token",
        emailVal = formLogin.find("#signin-email").val(),
        passVal = formLogin.find("#signin-password").val(),
        authData = { email: emailVal, password: passVal, rememberMe: true };
      //lscache.set("email", emailVal, 120);
      $.ajax({
        url: authUrl,
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        headers: { "x-api-version": "2.0" },
        data: JSON.stringify(authData),
        success: function(authdata) {
          if (authdata) {
            $(".login-signin")
              .html(
                $(".login-signin")
                  .html()
                  .replace("in", "out")
              )
              .removeClass("logged-out");
            formModal.removeClass("is-visible");
            //lscache.set("token", authdata.token, 120);
            setAuthToken(authdata.token);
            $.getScript("js/index.js");

            $(".userName").text(getUserName());

            //getUserInfo(emailVal);
          }
        },
        error: function(err) {
          email.addClass("has-error");
          password
            .addClass("has-error")
            .siblings("span.invalid")
            .addClass("is-visible");
        }
      });
    }
    !re.test(email.val())
      ? email
          .addClass("has-error")
          .next("span")
          .addClass("is-visible")
      : email
          .removeClass("has-error")
          .next("span")
          .removeClass("is-visible");
    !password.val()
      ? password
          .addClass("has-error")
          .siblings("span")
          .addClass("is-visible")
      : password
          .removeClass("has-error")
          .siblings("span")
          .removeClass("is-visible");
  });

  /*
  function getUserInfo(emailVal) {
    //Get User
    getJson("https://advantage.bia.com/api/Users/GetUserByUserName/" + emailVal)
      .done(function(userdata) {
        $(".userName").text(userdata.name);
        lscache.set("name", userdata.name, 120);
        $.each(userdata.roles, function(index, roles) {
          if (roles == "Administrator") {
            lscache.set("role", roles, 120);
          }
        });
      })
      .fail(function(err) {
        console.log("Unable to reach API");
      });
  }
*/

  //On Forgot Password Submit
  formForgotPassword.find('input[type="submit"]').on("click", function(event) {
    event.preventDefault();
    var re = /\S+@\S+\.\S+/,
      forgot = formForgotPassword.find("#forgot-email");
    if (re.test(forgot.val())) {
      //Send Password
      var emailVal = formForgotPassword.find("#forgot-email").val(),
        forgotUrl =
          "https://advantage.bia.com/api/Users/ForgotPassword?Email=" +
          emailVal;
      $.ajax({
        url: forgotUrl,
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        headers: { "x-api-version": "2.0" },
        success: function(forgotdata) {
          hideLoadPanel($("#loadPanel"));
          $("#login-reset-password .login-form-message")
            .hide()
            .addClass("success")
            .text("Success! You will receive a link to create a new password.")
            .fadeIn();
        },
        error: function(err) {
          hideLoadPanel($("#loadPanel"));
          forgot
            .addClass("has-error")
            .siblings("span.invalid")
            .addClass("is-visible");
        }
      });
    }
    !re.test(forgot.val())
      ? forgot
          .addClass("has-error")
          .next("span")
          .addClass("is-visible")
      : forgot
          .removeClass("has-error")
          .next("span")
          .removeClass("is-visible");
  });

  //On Change Password Submit
  formChangePassword.find('input[type="submit"]').on("click", function(event) {
    event.preventDefault();
    var re = new RegExp(
        "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})"
      ),
      $confirmPass = formChangePassword.find("#confirm-password"),
      $newPass = formChangePassword.find("#new-password"),
      $target = $("#login-change-password");
    if (re.test($confirmPass.val()) && $confirmPass.val() == $newPass.val()) {
      //Send Password
      var newpass = formChangePassword.find("#new-password").val(),
        confirmpass = formChangePassword.find("#confirm-password").val(),
        newPassUrl =
          "https://advantage.bia.com/api/Users/ResetPassword?Email=" +
          getEmail() +
          "&Password=" +
          newpass +
          "&ConfirmPassword=" +
          confirmpass;
      resetPassword(newPassUrl, $target);
      //Validation
      if ($confirmPass.val() && $newPass.val()) {
        $confirmPass.val() != $newPass.val()
          ? $confirmPass
              .addClass("has-error")
              .siblings("span.invalid")
              .addClass("is-visible")
          : $confirmPass
              .removeClass("has-error")
              .siblings("span.invalid")
              .removeClass("is-visible");
        $confirmPass.val() != $newPass.val() && !re.test($confirmPass.val())
          ? $confirmPass
              .removeClass("has-error")
              .siblings("span.invalid")
              .removeClass("is-visible")
          : "";
      }
      !$newPass.val()
        ? $newPass
            .addClass("has-error")
            .next("span")
            .addClass("is-visible")
        : $newPass
            .removeClass("has-error")
            .next("span")
            .removeClass("is-visible");
      !re.test($confirmPass.val())
        ? $confirmPass
            .addClass("has-error")
            .next("span")
            .addClass("is-visible")
        : $confirmPass
            .removeClass("has-error")
            .next("span")
            .removeClass("is-visible");
    }
  });

  function resetPassword(newPassUrl) {
    $.ajax({
      url: newPassUrl,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      headers: { "x-api-version": "2.0" },
      beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken());
      },
      success: function(passdata, $target) {
        if (passdata) {
          $target
            .find(".login-form-message")
            .hide()
            .addClass("success")
            .text("Success! Your password has been updated.")
            .fadeIn();
        }
      },
      error: function(err) {
        console.log(err);
      }
    });
  }

  //On New User Submit
  formNewUser.find('input[type="submit"]').on("click", function(event) {
    event.preventDefault();
    var re = new RegExp(
        "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})"
      ),
      reEmail = /\S+@\S+\.\S+/,
      $name = formNewUser.find("#new-name"),
      $email = formNewUser.find("#new-email"),
      $confirmPass = formNewUser.find("#new-confirm"),
      $newPass = formNewUser.find("#new-password"),
      $phone = formNewUser.find("#new-phone");
    if (
      re.test($confirmPass.val()) &&
      $confirmPass.val() == $newPass.val() &&
      reEmail.test($email.val()) &&
      $name.val() &&
      $phone.val()
    ) {
      var url = "https://advantage.bia.com/api/Users/AddUser",
        data = [],
        name = formNewUser.find("#new-name").val(),
        email = formNewUser.find("#new-email").val(),
        confirmPass = formNewUser.find("#new-confirm").val(),
        newPass = formNewUser.find("#new-password").val(),
        country = "1",
        custID = $("#new-user-customer").dropdown("get value"),
        phone = formNewUser.find("#new-phone").val(),
        newpassVal = formChangePassword.find("#new-password").val(),
        confirmpassVal = formChangePassword.find("#confirm-password").val(),
        maproUser = formNewUser.find("#new-mapro-username").val(),
        maproPass = formNewUser.find("#new-mapro-password").val(),
        roleOptions = $("#new-roles").dropdown("get value"),
        roles = [],
        accessOptions = $("#new-access").dropdown("get value"),
        access = [],
        marketOptions = $("#new-markets").dropdown("get value"),
        markets = [];
      $.each(roleOptions, function(index, value) {
        roles.push(value);
      });
      $.each(accessOptions, function(index, value) {
        access.push({
          claimType: value,
          claimValue: "True"
        });
      });
      $.each(marketOptions, function(index, value) {
        markets.push({
          marketNbr: value,
          tv: true,
          radio: true
        });
      });
      data.push({
        name: name,
        email: email,
        password: newPass,
        confirmPassword: confirmPass,
        countryCode: country,
        phoneNumber: phone,
        customerId: custID,
        maproUserName: maproUser,
        maproPassword: maproPass,
        claims: access,
        markets: markets,
        roles: roles
      });
      showLoadPanel($("#loadPanel"), $(".cd-main"), true);
      $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        headers: { "x-api-version": "2.0" },
        data: JSON.stringify(data[0]),
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Bearer " + authToken());
        },
        success: function(changedata) {
          hideLoadPanel($("#loadPanel"));
          $(".login-form-message")
            .hide()
            .addClass("success")
            .text("Success! This user has been created.")
            .fadeIn();
          setTimeout(function() {
            location.reload();
          }, 2000);
        },
        error: function(err) {
          console.log(err);
          hideLoadPanel($("#loadPanel"));
        }
      });
    }
    // New User Validation
    if ($confirmPass.val() && $newPass.val()) {
      $confirmPass.val() != $newPass.val()
        ? $confirmPass
            .addClass("has-error")
            .siblings("span.invalid")
            .addClass("is-visible")
        : $confirmPass.removeClass("has-error");
      $confirmPass.val() != $newPass.val() && !re.test($confirmPass.val())
        ? $confirmPass
            .removeClass("has-error")
            .siblings("span.invalid")
            .removeClass("is-visible")
        : "";
    }
    !$newPass.val()
      ? $newPass
          .addClass("has-error")
          .next("span")
          .addClass("is-visible")
      : $newPass.removeClass("has-error");
    !reEmail.test($email.val())
      ? $email
          .addClass("has-error")
          .next("span")
          .addClass("is-visible")
      : $email.removeClass("has-error");
    !$name.val()
      ? $name
          .addClass("has-error")
          .next("span")
          .addClass("is-visible")
      : $name.removeClass("has-error");
    !re.test($confirmPass.val())
      ? $confirmPass
          .addClass("has-error")
          .next("span")
          .addClass("is-visible")
      : $confirmPass.removeClass("has-error");
    !$phone.val()
      ? $phone
          .addClass("has-error")
          .next("span")
          .addClass("is-visible")
      : "";
    setTimeout(function() {
      $name.next("span").removeClass("is-visible");
      $confirmPass.siblings("span.invalid").removeClass("is-visible");
      $confirmPass.next("span").removeClass("is-visible");
      $phone.next("span").removeClass("is-visible");
      $newPass.next("span").removeClass("is-visible");
      $email.next("span").removeClass("is-visible");
    }, 3000);
  });

  function getUsers() {
    //Get Users
    getJson("https://advantage.bia.com/api/Users/GetAllUsers")
      .done(function(userdata) {
        var $target = $("#user-id");
        $target.empty();
        $target.append('<option value="">Users</option>');
        $.each(userdata, function(index, value) {
          if (value.id != "c66a53bb-ef96-4c4c-8454-f7d68b3cb362") {
            var option = $("<option />");
            option
              .addClass("button")
              .html(value.name)
              .val(value.id)
              .attr("data-email", value.email);
            $target.append(option);
          }
        });
        $target.dropdown({
          onChange: function(value, text, $selectedItem) {
            var currentUser = value,
              currentEmail,
              currentPhone,
              currentCustomer,
              currentMaproUser,
              currentMaproPass,
              currentAccess = [],
              currentRoles = [],
              currentMarkets = [];
            $.each(userdata, function(index, value) {
              if (value.id == currentUser) {
                currentEmail = value.email;
                currentPhone = value.phoneNumber;
                currentCustomer = value.customerId;
                currentMaproUser = value.maproUserName;
                currentMaproPass = value.maproPassword;
                $.each(value.claims, function(index1, claims) {
                  if (claims.claimValue == "True") {
                    currentAccess.push(claims.claimType);
                  }
                });
                $.each(value.roles, function(index1, roles) {
                  currentRoles.push(roles);
                });
                if (currentCustomer != null) {
                  //Get Customer Markets
                  getJson(
                    "https://advantage.bia.com/api/CustomerMarkets/" +
                      currentCustomer,
                    value
                  )
                    .done(function(mktdata) {
                      var $target = $("#user-markets");
                      $target.empty();
                      $target.siblings(".ui.label").remove();
                      $target.append('<option value="">Markets</option>');
                      $.each(mktdata, function(index1, markets) {
                        var option = $("<option />");
                        option
                          .addClass("button")
                          .html(markets.marketName)
                          .val(markets.marketNbr);
                        $target.append(option);
                      });
                      $target.dropdown();
                      $.each(value.markets, function(index1, markets) {
                        currentMarkets.push(markets.marketNbr.toString());
                      });
                      setTimeout(function() {
                        $target.dropdown("set exactly", currentMarkets);
                      }, 10);
                      mktSelectAll();
                    })
                    .fail(function(err) {
                      console.log("Unable to reach API");
                    });
                } else {
                  //Get User Markets
                  var $target = $("#user-markets");
                  $target.empty();
                  $target.siblings(".ui.label").remove();
                  $target.append('<option value="">Markets</option>');
                  $.each(value.markets, function(index1, markets) {
                    var option = $("<option />");
                    option
                      .addClass("button")
                      .html(markets.marketName)
                      .val(markets.marketNbr);
                    $target.append(option);
                  });
                  $target.dropdown();
                  $.each(value.markets, function(index1, markets) {
                    currentMarkets.push(markets.marketNbr.toString());
                  });
                  $target.dropdown("set exactly", currentMarkets);
                  mktSelectAll();
                }

                function mktSelectAll() {
                  //Select All
                  $("#markets-all").checkbox({
                    onChecked() {
                      const options = $("#user-markets > option")
                        .toArray()
                        .map(obj => obj.value);
                      $("#user-markets").dropdown("set exactly", options);
                    },
                    onUnchecked() {
                      //$("#user-markets").siblings('.ui.label').remove();
                      $("#user-markets").dropdown("clear");
                    }
                  });
                }
              }
            });
            $("#user-markets, #access, #roles, #customer")
              .dropdown()
              .removeClass("disabled");
            $("#access-all, #markets-all")
              .checkbox()
              .removeClass("disabled");
            $(
              "#manage-password, #manage-confirm, #manage-phone, #new-mapro-username, #new-mapro-password, #delete-user, #manage-mapro-username, #manage-mapro-password, #update-user"
            ).removeClass("disabled");
            $("#roles").dropdown("set exactly", currentRoles);
            $("#access").dropdown("set exactly", currentAccess);
            //Select All
            $("#access-all").checkbox({
              onChecked() {
                const options = $("#access > option")
                  .toArray()
                  .map(obj => obj.value);
                $("#access").dropdown("set exactly", options);
              },
              onUnchecked() {
                $("#access").dropdown("clear");
              }
            });
            $("#manage-email").attr("placeholder", currentEmail);
            $("#manage-phone").val(currentPhone);
            currentCustomer == null
              ? $("#customer").dropdown("clear")
              : $("#customer").dropdown("set selected", currentCustomer);
            $("#manage-mapro-username").val(currentMaproUser);
            $("#manage-mapro-password").val(currentMaproPass);
          }
        });
      })
      .fail(function(err) {
        console.log("Unable to reach API");
      });
  }

  //On Delete User Submit
  formManageUser.find("#delete-user").on("click", function(event) {
    event.preventDefault();
    var currentUser = users.children("option:selected").val(),
      delUrl = "https://advantage.bia.com/api/Users/" + currentUser;
    showLoadPanel($("#loadPanel"), $(".cd-main"), true);
    $.ajax({
      url: delUrl,
      type: "DELETE",
      dataType: "json",
      contentType: "application/json",
      headers: { "x-api-version": "2.0" },
      beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken());
      },
      success: function(changedata) {},
      error: function(err) {
        hideLoadPanel($("#loadPanel"));
        if (err.status == 200) {
          $(".login-form-message")
            .hide()
            .addClass("success")
            .text("Success! This user has been deleted.")
            .fadeIn();
          setTimeout(function() {
            location.reload();
          }, 2000);
        }
      }
    });
  });

  //On Update User Submit
  formManageUser.find("#update-user").on("click", function(event) {
    event.preventDefault();
    var currentUser = users.children("option:selected").val(),
      re = new RegExp(
        "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})"
      ),
      $confirmPass = formManageUser.find("#manage-confirm"),
      $newPass = formManageUser.find("#manage-password"),
      $custID = formManageUser.find("#customer"),
      $phone = formManageUser.find("#manage-phone"),
      $target = $("#login-manage-user"),
      currentEmail = users.children("option:selected").attr("data-email"),
      confirmPass = formManageUser.find("#confirm").val(),
      newPass = formManageUser.find("#password").val();

    if ($newPass.val()) {
      //Validate password
      $confirmPass.val() != $newPass.val()
        ? $confirmPass
            .addClass("has-error")
            .siblings("span.invalid")
            .addClass("is-visible")
        : $confirmPass
            .removeClass("has-error")
            .siblings("span.invalid")
            .removeClass("is-visible");

      $confirmPass.val() != $newPass.val() && !re.test($confirmPass.val())
        ? $confirmPass
            .removeClass("has-error")
            .siblings("span.invalid")
            .removeClass("is-visible")
        : "";

      !$newPass.val()
        ? $newPass
            .addClass("has-error")
            .next("span")
            .addClass("is-visible")
        : $newPass
            .removeClass("has-error")
            .next("span")
            .removeClass("is-visible");

      !re.test($confirmPass.val())
        ? $confirmPass
            .addClass("has-error")
            .next("span")
            .addClass("is-visible")
        : $confirmPass
            .removeClass("has-error")
            .next("span")
            .removeClass("is-visible");
      setTimeout(function() {
        $confirmPass.siblings("span.invalid").removeClass("is-visible");
        $confirmPass.next("span").removeClass("is-visible");
        $newPass.next("span").removeClass("is-visible");
      }, 3000);
    }

    //Update User
    var url = "https://advantage.bia.com/api/Users/UpdateUser",
      data = [],
      name = $("#user-id").dropdown("get text"),
      confirmPass = formManageUser.find("#manage-confirm").val(),
      newPass = formManageUser.find("#manage-password").val(),
      country = "1",
      custID = $("#customer").dropdown("get value"),
      phone = formManageUser.find("#manage-phone").val(),
      maproUser = formManageUser.find("#manage-mapro-username").val(),
      maproPass = formManageUser.find("#manage-mapro-password").val(),
      roleOptions = $("#roles").dropdown("get value"),
      roles = [],
      accessOptions = $("#access").dropdown("get value"),
      access = [],
      marketOptions = $("#user-markets").dropdown("get value"),
      markets = [];
    $.each(roleOptions, function(index, value) {
      roles.push(value);
    });
    $.each(accessOptions, function(index, value) {
      access.push({
        claimType: value,
        claimValue: "True"
      });
    });
    $.each(marketOptions, function(index, value) {
      markets.push({
        marketNbr: value,
        tv: true,
        radio: true
      });
    });
    data.push({
      name: name,
      email: currentEmail,
      password: newPass == "" ? null : newPass,
      confirmPassword: confirmPass == "" ? null : confirmPass,
      countryCode: country,
      phoneNumber: phone,
      customerId: custID,
      maproUserName: maproUser,
      maproPassword: maproPass,
      claims: access,
      markets: markets,
      roles: roles
    });
    showLoadPanel($("#loadPanel"), $(".cd-main"), true);
    $.ajax({
      url: url,
      type: "PUT",
      dataType: "json",
      contentType: "application/json",
      headers: { "x-api-version": "2.0" },
      data: JSON.stringify(data[0]),
      beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken());
      },
      success: function(changedata) {},
      error: function(err) {
        hideLoadPanel($("#loadPanel"));
        if (err.status == 200) {
          $(".login-form-message")
            .hide()
            .addClass("success")
            .text("Success! This user has been updated.")
            .fadeIn();
          setTimeout(function() {
            location.reload();
          }, 2000);
        }
      }
    });

    /*

*/
    /*                                      
    // New User Validation
    if ($confirmPass.val() && $newPass.val()) {
      $confirmPass.val() != $newPass.val()
        ? $confirmPass
            .addClass("has-error")
            .siblings("span.invalid")
            .addClass("is-visible")
        : $confirmPass
            .removeClass("has-error")
            .siblings("span.invalid")
            .removeClass("is-visible");
      $confirmPass.val() != $newPass.val() && !re.test($confirmPass.val())
        ? $confirmPass
            .removeClass("has-error")
            .siblings("span.invalid")
            .removeClass("is-visible")
        : "";
    }
    !$newPass.val()
      ? $newPass
          .addClass("has-error")
          .next("span")
          .addClass("is-visible")
      : $newPass
          .removeClass("has-error")
          .next("span")
          .removeClass("is-visible");
    !reEmail.test($email.val())
      ? $email
          .addClass("has-error")
          .next("span")
          .addClass("is-visible")
      : $email
          .removeClass("has-error")
          .next("span")
          .removeClass("is-visible");
    !$name.val()
      ? $name
          .addClass("has-error")
          .next("span")
          .addClass("is-visible")
      : $name
          .removeClass("has-error")
          .next("span")
          .removeClass("is-visible");
    !re.test($confirmPass.val())
      ? $confirmPass
          .addClass("has-error")
          .next("span")
          .addClass("is-visible")
      : $confirmPass
          .removeClass("has-error")
          .next("span")
          .removeClass("is-visible");
  */
  });

  //On New Customer Submit
  formNewCustomer.find('input[type="submit"]').on("click", function(event) {
    event.preventDefault();
    var $customer = formNewCustomer.find("#new-name");
    if ($customer.val()) {
      var url = "https://advantage.bia.com/api/Customers",
        data = [],
        name = $customer.val(),
        address = formNewCustomer.find("#new-address").val(),
        city = formNewCustomer.find("#new-city").val(),
        state = $("#new-state").dropdown("get value"),
        zip = formNewCustomer.find("#new-zip").val(),
        marketOptions = $("#customer-markets").dropdown("get value"),
        markets = [];
      $.each(marketOptions, function(index, value) {
        markets.push({
          marketNbr: value,
          tv: true,
          radio: true
        });
      });
      data.push({
        name: name,
        address: address,
        city: city,
        state: state,
        zipcode: zip,
        api: true,
        dashboard: true,
        markets: markets
      });
      showLoadPanel($("#loadPanel"), $(".cd-main"), true);
      $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        headers: { "x-api-version": "2.0" },
        data: JSON.stringify(data[0]),
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Bearer " + authToken());
        },
        success: function(newcustomerdata) {
          hideLoadPanel($("#loadPanel"));
          $(".login-form-message")
            .hide()
            .addClass("success")
            .text("Success! This customer has been created.")
            .fadeIn();
          setTimeout(function() {
            location.reload();
          }, 2000);
        },
        error: function(err) {
          hideLoadPanel($("#loadPanel"));
          console.log(err);
        }
      });
    }
  });

  //On Delete Customer Submit
  formManageCustomer.find("#delete-customer").on("click", function(event) {
    event.preventDefault();
    var currentUser = $("#manage-customer")
        .children("option:selected")
        .val(),
      delUrl = "https://advantage.bia.com/api/Customers/" + currentUser;
    showLoadPanel($("#loadPanel"), $(".cd-main"), true);
    $.ajax({
      url: delUrl,
      type: "DELETE",
      dataType: "json",
      contentType: "application/json",
      headers: { "x-api-version": "2.0" },
      beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken());
      },
      success: function(deletedata) {
        hideLoadPanel($("#loadPanel"));
        $(".login-form-message")
          .hide()
          .addClass("success")
          .text("Success! This customer has been deleted.")
          .fadeIn();
        setTimeout(function() {
          location.reload();
        }, 2000);
      },
      error: function(err) {
        console.log(err);
      }
    });
  });

  //On Update Customer Submit
  formManageCustomer.find("#update-customer").on("click", function(event) {
    event.preventDefault();
    var customerId = $("#manage-customer").dropdown("get value"),
      url = "https://advantage.bia.com/api/Customers/" + customerId,
      data = [],
      name = formManageCustomer.find("#name").val(),
      address = formManageCustomer.find("#address").val(),
      city = formManageCustomer.find("#city").val(),
      state = $("#state").dropdown("get value"),
      zip = formManageCustomer.find("#zip").val(),
      marketOptions = $("#manage-customer-markets").dropdown("get value"),
      markets = [];
    $.each(marketOptions, function(index, value) {
      markets.push({
        marketNbr: value,
        tv: true,
        radio: true
      });
    });
    data.push({
      customerId: customerId,
      name: name,
      address: address,
      city: city,
      state: state,
      zipcode: zip,
      api: true,
      dashboard: true,
      markets: markets
    });
    showLoadPanel($("#loadPanel"), $(".cd-main"), true);
    $.ajax({
      url: url,
      type: "PUT",
      dataType: "json",
      contentType: "application/json",
      headers: { "x-api-version": "2.0" },
      data: JSON.stringify(data[0]),
      beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken());
      },
      success: function(newcustomerdata) {
        hideLoadPanel($("#loadPanel"));
        $(".login-form-message")
          .hide()
          .addClass("success")
          .text("Success! This customer has been updated.")
          .fadeIn();
        setTimeout(function() {
          location.reload();
        }, 2000);
      },
      error: function(err) {
        hideLoadPanel($("#loadPanel"));
        console.log(err);
      }
    });
  });

  //IE9 placeholder fallback
  if (!Modernizr.input.placeholder) {
    $("[placeholder]")
      .focus(function() {
        var input = $(this);
        if (input.val() == input.attr("placeholder")) {
          input.val("");
        }
      })
      .blur(function() {
        var input = $(this);
        if (input.val() == "" || input.val() == input.attr("placeholder")) {
          input.val(input.attr("placeholder"));
        }
      })
      .blur();
    $("[placeholder]")
      .parents("form")
      .submit(function() {
        $(this)
          .find("[placeholder]")
          .each(function() {
            var input = $(this);
            if (input.val() == input.attr("placeholder")) {
              input.val("");
            }
          });
      });
  }
});

// Move cursor to end of text
$.fn.putCursorAtEnd = function() {
  return this.each(function() {
    if (this.setSelectionRange) {
      var len = $(this).val().length * 2;
      this.focus();
      this.setSelectionRange(len, len);
    } else {
      $(this).val($(this).val());
    }
  });
};
