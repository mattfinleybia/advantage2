$(document).on("navSelected", "a.market-report.selected", function() {
  var $dgInitialized = false;

  $(".select-item.subvert").removeClass("enable");
  $(".select-item.market.main-market, .navbar-brand").addClass("enable");

  loadReport();

  function loadReport() {
    showLoadPanel($("#loadPanel"), $(".cd-main"), true);

    var mktSelected = getSelectedMarket();

    $(".select-item.market").on("change.mktReport", function() {
      if ($(".market-report").hasClass("selected")) {
        loadReport();
      } else {
        $(".select-item.market").off("change.mktReport");
      }
    });

    $("#fmr-title").text("Full Market Report - " + mktSelected.text());

    // get the selected market nbr value
    $selectedMarketNbr = mktSelected.val();

    getJson(
      getBaseHref() +
        "api/MediaAdView/GetFullMarketReport/" +
        $selectedMarketNbr +
        "/2018"
    )
      .done(function(data) {
        if ($dgInitialized == true) {
          $("#marketreport").dxDataGrid("option", "dataSource", data);
        } else {
          $("#marketreport").dxDataGrid({
            dataSource: data,
            allowColumnReordering: true,
            allowColumnResizing: true,
            columnAutoWidth: true,
            columnHidingEnabled: false,
            columnFixing: true,
            showBorders: true,
            rowAlternationEnabled: true,
            columnChooser: {
              allowSearch: true,
              mode: "select",
              enabled: true
            },
            export: {
              enabled: true,
              fileName: "Full Market Report",
              allowExportSelectedData: false
            },
            groupPanel: {
              visible: true
            },
            headerFilter: {
              visible: true
            },
            searchPanel: {
              width: 240,
              highlightCaseSensitive: true,
              highlightSearchText: true,
              visible: true
            },
            columns: [
              {
                dataField: "year",
                caption: "Year"
              },
              {
                dataField: "marketName",
                caption: "Market Name"
              },
              {
                dataField: "verticalName",
                caption: "Vertical"
              },
              {
                caption: "Sub-Vertical",
                dataField: "subVerticalName"
              },
              {
                caption: "Cable TV",
                dataField: "cableTv",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "Direct Mail",
                dataField: "directMail",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "Email",
                dataField: "email",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "Internet Yellow Pages",
                dataField: "internetYellowPages",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "Magazines Online",
                dataField: "magazinesOnline",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "Magazines Print",
                dataField: "magazinesPrint",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "Mobile",
                dataField: "mobile",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "Newspapers Online",
                dataField: "newspapersOnline",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "Newspapers Print",
                dataField: "newspapersPrint",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "Online",
                dataField: "online",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "Out-Of-Home",
                dataField: "outOfHome",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "Print Yellow Pages",
                dataField: "printYellowPages",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "Radio Online",
                dataField: "radioOnline",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "Radio OTA",
                dataField: "radioOta",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "TV Online",
                dataField: "tvOnline",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "TV OTA",
                dataField: "tvOta",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "Online Search",
                dataField: "onlineSearch",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "Online Video Display",
                dataField: "onlineVideoDisplay",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "Online Other Display",
                dataField: "onlineOtherDisplay",
                allowFiltering: false,
                allowGrouping: false
              },
              {
                caption: "Online Classified Verticals",
                dataField: "onlineClassifiedsVerticals",
                allowFiltering: false,
                allowGrouping: false
              }
            ],
            summary: {
              groupItems: [
                {
                  column: "cableTv",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "directMail",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "email",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "internetYellowPages",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "magazinesOnline",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "magazinesPrint",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "mobile",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "newspapersOnline",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "newspapersPrint",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "online",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "outOfHome",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "printYellowPages",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "radioOnline",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "radioOta",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "tvOnline",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "tvOta",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "onlineSearch",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "onlineVideoDisplay",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "onlineOtherDisplay",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                },
                {
                  column: "onlineClassifiedsVerticals",
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "{0}",
                  showInGroupFooter: true
                }
              ]
            },
            onInitialized: function(e) {
              $dgInitialized = true;
              hideLoadPanel($("#loadPanel"));
            },
            onOptionChanged: function(e) {
              hideLoadPanel($("#loadPanel"));
            }
          });
        }
      })
      .fail(function(err) {
        hideLoadPanel($("#loadPanel"));
      });
  }
});
