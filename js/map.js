/*global $, console, L */
$(document).ready(function () {
    'use strict';
    function updateMap() {
        setTimeout(function () {
            if ($("#nav").find('a[data-menu="index"]').hasClass("selected")) {
               // Initialize MAP
                var $layer,
                    $geoLat = 40.7127837,
                    $geoLng = -74.0059413,
                    map = L.map("map", {
                        center: [$geoLat, $geoLng],
                        zoom: 7,
                        attributionControl: false,
                        layers: [
                            L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
                                attribution: ""
                            })
                        ]
                    });
                $.getJSON(
                    "https://s3-us-west-2.amazonaws.com/s.cdpn.io/532833/1.geo.json",
                    function (geojsonInit) {
                        $layer = L.geoJson(geojsonInit, {
                            onEachFeature: function (feature, layer) {
                                layer.bindPopup(feature.geographicRegion);
                            }
                        }).addTo(map);
                    }
                );
            }
        }, 100);
    }
});
