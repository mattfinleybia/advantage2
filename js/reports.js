$(document).on("navSelected", "a.reports.selected", function() {
  showLoadPanel($("#loadPanel"), $(".cd-main"), true);

  $(".select-item.subvert").removeClass("enable");
  $(".select-item.market.main-market, .navbar-brand").addClass("enable");

  getJson(
    getBaseHref() + "api/Dashboard/GetPublishedSlug/media-industry-reports"
  )
    .done(function(data) {
      // add the returned data to the div
      $("#reportsContent").html(data.content.rendered);

      // set the base href for all img tags
      $("#reportsContent img").attr("src", function(index, src) {
        return "https://dashboard.biakelsey.com/" + src;
      });

      $("#reportsContent a[href^='/']").attr("href", function(index, src) {
        //if (src.toString().toLowerCase().indexOf('https://') >= 0)
        //    return src;

        return "https://dashboard.biakelsey.com/" + src;
      });

      // remove all class elements
      // $('#reportsContent').find("*").prop("class", "");

      // set default classes
      var reportsContent = $("#reportsContent");
      reportsContent
        .find(".vc_row")
        .addClass("row")
        .removeClass("vc_row");
      reportsContent
        .find(".vc_col-sm-12")
        .addClass("col-lg-12")
        .removeClass("vc_col-sm-12");
      reportsContent
        .find(".vc_col-sm-4")
        .addClass("col-lg-4")
        .removeClass("vc_col-sm-4");
      reportsContent.find(".vc_empty_space").remove();

      hideLoadPanel($("#loadPanel"));
    })
    .fail(function(err) {
      hideLoadPanel($("#loadPanel"));
    });
});
