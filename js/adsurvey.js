$(document).on("navSelected", "a.survey.selected", function() {
  showLoadPanel($("#loadPanel"), $(".cd-main"), true);

  $(".select-item.subvert").removeClass("enable");
  $(".select-item.market.main-market, .navbar-brand").addClass("enable");

  getJson(getBaseHref() + "api/Dashboard/GetPublishedSlug/advertiser-research")
    .done(function(data) {
      // add the returned data to the div
      $("#adsurveycontent").html(data.content.rendered);

      // set the base href for all img tags
      $("#adsurveycontent img").attr("src", function(index, src) {
        return "https://dashboard.biakelsey.com/" + src;
      });

      $("#adsurveycontent a[href^='/']").attr("href", function(index, src) {
        //if (src.toString().toLowerCase().indexOf('https://') >= 0)
        //    return src;

        return "https://dashboard.biakelsey.com/" + src;
      });

      // remove all class elements
      // $('#adsurveycontent').find("*").prop("class", "");

      var adSurveyContent = $("#adsurveycontent");
      adSurveyContent
        .find(".vc_row")
        .addClass("row")
        .removeClass("vc_row");
      adSurveyContent
        .find(".vc_col-sm-12")
        .addClass("col-lg-12")
        .removeClass("vc_col-sm-12");
      adSurveyContent
        .find(".vc_col-sm-8")
        .addClass("col-lg-8")
        .removeClass("vc_col-sm-8");
      adSurveyContent
        .find(".vc_col-sm-4")
        .addClass("col-lg-4")
        .removeClass("vc_col-sm-4");
      adSurveyContent
        .find(".vc_col-sm-3")
        .addClass("col-lg-3")
        .removeClass("vc_col-sm-3");

      hideLoadPanel($("#loadPanel"));
    })
    .fail(function(err) {
      hideLoadPanel($("#loadPanel"));
    });
});
