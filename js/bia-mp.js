function validateUserName(userName) {
  var name = userName.replace("@", "$").substring(0, 19);
  while (name.indexOf(".") !== -1) {
    name = name.replace(".", "_");
  }

  return name;
}

function mapro_connect(uName, uPwd) {
  mythinrdp = GetThinRDP("https://biakelsey.datamark.live/mapro/", true);
  mythinrdp.connect({
    postPage: "connection.html",
    exitURL: "https://advantage.bia.com",
    centered: true,
    overrideDefaults: false, // not applicable in ThinRDP Workstation
    showOnStart: false,
    divId: "deskdiv",
    reconnectOnResize: true, // -- Forces the app to restart when a resize event occurs, by default is "false"
    //*** Tab General ***
    computer: "74.114.163.37", // not applicable in ThinRDP Workstation
    username: validateUserName(uName),
    password: uPwd,
    askForCredentials: false,
    //*** Tab Program ***
    startprg: 1,
    command: "C:\\BIADATA\\MASTER\\Master.exe",
    //*** Tab Experience ***
    experience: {
      enableRemoteFx: true,
      enableTouchRedirection: false,
      desktopbackground: false,
      visualstyles: false,
      menuwindowanimation: false,
      fontsmoothing: true,
      showwindowcontent: false,
      desktopcomposition: false
    },

    //*** Tab Advanced ***
    unicodeKeyboard: true,
    kbdLayout: 1033, // US keyboard
    console: false,
    wscompression: true,
    relativeTouch: true,
    disableNLA: false,
    disableExtKeys: false,
    //*** Intermediate Disk ***
    disk: {
      enabled: true,
      name: "Transfer-" + validateUserName(uName),
      autodownload: true
    },
    //*** Tab Printer ***
    printer: {
      enabled: true,
      setasdefault: true,
      name: "PDF Remote Printer",
      driver: "Microsoft XPS Document Writer V4"
    },
    sound: {
      enabled: false,
      quality: -1,
      channels: {
        minValue: 1,
        maxValue: 2
      },
      samplesPerSec: {
        minValue: 11025,
        maxValue: 44100
      },
      bitsPerSample: {
        minValue: 4,
        maxValue: 16
      }
    },
    events: {
      onServerConnecting: function(reconnecting) {
        //-- formerly "establishingConnection" event binding
      },
      onServerConnect: function(obj) {},
      onQueryDisconnect: function(callback) {},
      onInteractionRequired: function(cmd) {
        //$.unblockUI();
      },
      onServerConnectionError: function(errMessage) {
        //--formerly "disconnectConfirmRequest" event bindig
        alert("connect error: " + errMessage);
      },
      onServerDisconnect: function() {
        //--formerly "serverDisconnect" event bindig
      },
      onExecResult: function(cmd) {
        //--formerly "execResult" event bindig
      },
      // onFileReady: function (sURL) { }, // custom printing and automatic download
      onSessionEnd: function(message) {
        //--formerly "sessionEnd" event bindig
      },
      onSessionStart: function() {
        //--formerly "sessionStart" event bindig
      }
    },
    // Toolbar customization
    createToolbar: true,
    toolbarVisible: true,
    toolbarRestrictions: [
      /*"actionsMenuBtn", //"Actions"
             "actionsMenuBtn.refresh", //"Refresh"
             "actionsMenuBtn.ssnShareBtn", //"Share session"
             "actionsMenuBtn.sendKeysBtn", //"Send Keys..."
             "actionsMenuBtn.sendKeysBtn.ctrlAltDelBtn", //"Ctrl + Alt + Del"
             "actionsMenuBtn.sendKeysBtn.ctrlEscBtn", //"Ctrl + Esc"
             "actionsMenuBtn.sendKeysBtn.shiftCtrlEscBtn", //"Shift + Ctrl + Esc"
             "actionsMenuBtn.sendKeysBtn.windowsExplorerBtn", //"Shell Explorer"
             "actionsMenuBtn.sendKeysBtn.runBtn", //"Run"
             "actionsMenuBtn.sendKeysBtn.altTabBtn", //"Alt + Tab"
             "actionsMenuBtn.sendKeysBtn.altShiftTabBtn", //"Alt + Shift + Tab"
             "actionsMenuBtn.sendKeysBtn.altEscBtn", //"Alt + Esc"
             "actionsMenuBtn.sendKeysBtn.leftWinBtn", //"Left Win Key"
             "actionsMenuBtn.sendKeysBtn.rightWinBtn", //"Right Win Key"
             "actionsMenuBtn.viewOptionsBtn", //"View params & layout"
             "fileMenuBtn", //"File transfer"*/
      "fileMenuBtn.fileManBtn" //"File Manager"
      /*"fileMenuBtn.uploadBtn", //"Upload"
             "fileMenuBtn.downloadBtn", //"Download"
             "optionsMenuBtn", // "Options"
             "optionsMenuBtn.scaleBtn", //"Scale"
             "optionsMenuBtn.imgQualityBtn", //"Image Quality"
             "optionsMenuBtn.imgQualityBtn.imgQHighestBtn", //"Highest"
             "optionsMenuBtn.imgQualityBtn.imgQOptimalBtn", //"Optimal"
             "optionsMenuBtn.imgQualityBtn.imgQGoodBtn", //"Good"
             "optionsMenuBtn.imgQualityBtn.imgQPoorBtn", //"Poor"
             "optionsMenuBtn.keyboardMode", //"Disable Shortcuts"
             "disconnectBtn", //"Disconnect"*/
    ]
  });
}
