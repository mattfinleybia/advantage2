$(function() {
  "use strict";
  setTimeout(function() {
    var navigationContainer = $("#cd-nav"),
      mainNavigation = navigationContainer.find("#cd-main-nav ul");
    $(".cd-nav-trigger, .is-fixed").hover(function() {
      var $this = $(this);
      if (!$this.find("a").hasClass("disabled")) {
        $this.toggleClass("menu-is-open");
        if ($this.hasClass("is-fixed")) {
          $this.prev().toggleClass("nav-trigger-hover");
        }
        mainNavigation
          .off(
            "webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend"
          )
          .toggleClass("is-visible");
      }
    });

    $(".cd-nav-trigger").on("click", function(e) {
      e.preventDefault();
    });

    //toggle main left nav
    $(".main-nav.navbar-toggle").on("click", function() {
      $(".sidebar, .main-panel, .navbar, .dxchart").toggleClass("collapse");
      if ($("#locSpend").length > 0) {
        var locSpend = $("#locSpend").dxPieChart("instance"),
          natSpend = $("#natSpend").dxPieChart("instance"),
          locForcast = $("#locForcast").dxChart("instance");
        setTimeout(function() {
          locSpend.render();
          natSpend.render();
          locForcast.render();
        }, 300);
      }
      if ($("#natForcast17").length > 0) {
        var natForcast17 = $("#natForcast17").dxPieChart("instance"),
          natForcast21 = $("#natForcast21").dxPieChart("instance");
        setTimeout(function() {
          natForcast17.render();
          natForcast21.render();
        }, 300);
      }
      if ($("#vertSpend").length > 0) {
        var vertSpend = $("#vertSpend").dxPieChart("instance");
        setTimeout(function() {
          vertSpend.render();
        }, 300);
      }
      if ($("#subvertSpend").length > 0) {
        var subvertSpend = $("#subvertSpend").dxPieChart("instance"),
          platformSpend = $("#platformSpend").dxPieChart("instance");
        setTimeout(function() {
          subvertSpend.render();
          platformSpend.render();
        }, 300);
      }
    });

    //toggle mobile nav
    $(".mobile-nav.navbar-toggle").on("click", function() {
      $(".sidebar").toggleClass("sidebar-show");
      $(".cd-section").toggleClass("sidebar-shift");
      $("#cd-main-nav ul").toggleClass("nav-shift");
    });
  }, 500);

  var sidebar_container,
    $sidebar = $(".sidebar"),
    image_src = $sidebar.data("image");
  if (image_src !== undefined) {
    sidebar_container =
      '<div class="sidebar-background" style="background-image: url(' +
      image_src +
      ') "/>';
    $sidebar.append(sidebar_container);
  }
});
