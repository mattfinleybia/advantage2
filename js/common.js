var userObj = new Object();

// set this value to false when running out of the
// development environment
var coreDebug = false;

function getBaseHref() {
  if (coreDebug == true) return "http://localhost:49696/";

  return "https://advantage.bia.com/";
}

// bearer token
function authToken() {
  return lscache.get("token");
}

function setAuthToken(token) {
  lscache.set("token", token, 120);
}

function getUserName() {
  var usrInfo = jwt_decode(lscache.get("token"));
  if ($.isEmptyObject(usrInfo)) return "Unknown";

  return usrInfo["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"];
}

function getEmail() {
  var usrInfo = jwt_decode(lscache.get("token"));
  if ($.isEmptyObject(usrInfo)) return "Invalid";

  return usrInfo.sub;
}

function IsInRole(role) {
  var usrInfo = jwt_decode(lscache.get("token"));
  if ($.isEmptyObject(usrInfo)) return false;

  var roles =
    usrInfo["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
  if (Array.isArray(roles)) {
    if (roles.findIndex(x => x == role) == -1) return false;
  } else {
    if (typeof roles == "string" && roles.toLowerCase() != role.toLowerCase()) {
      return false;
    }
  }

  return true;
}

function getSelectedMarket() {
  mktSelected = $(".select-item.market").find("option:selected");

  return mktSelected;
}

function getJson(strUrl) {
  return $.ajax({
    url: strUrl,
    async: true,
    type: "GET",
    dataType: "json",
    contentType: "application/json",
    headers: { "x-api-version": "2.0" },
    beforeSend: function(xhr) {
      xhr.setRequestHeader("Authorization", "Bearer " + authToken());
    }
  });
}

function launchMapPro() {
  // don't like this at all but it is what it is at this point
  var usrInfo = jwt_decode(lscache.get("token"));
  if ($.isEmptyObject(usrInfo)) return;

  getJson(
    "https://advantage.bia.com/api/Users/GetUserByUserName/" + usrInfo.sub
  )
    .done(function(data) {
      // I do not like this at all.
      // create a sharedobject to pass the credentials we need
      userObj.name = data.maproUserName;
      userObj.password = data.maproPassword;

      // we have to do it this way because mapro requires an older version
      // of jquery, really stupid but no update coming.
      window.open("mp.html");
    })
    .fail(function(err) {
      console.log("Unable to launch mapro");
    });
}

// TODO
// we should do some safety checking here to make sure that the
// properties exists

function setupNavOptions() {
  var usrInfo = jwt_decode(lscache.get("token"));
  if ($.isEmptyObject(usrInfo)) {
    return;
  }

  if (
    usrInfo.hasOwnProperty("Mapro") &&
    usrInfo.Mapro.toLowerCase() == "true"
  ) {
    $(".nav a.mapro").removeClass("disabled");
  }

  if (
    usrInfo.hasOwnProperty("ReportWebinars") &&
    usrInfo.ReportWebinars.toLowerCase() == "true"
  ) {
    $(".nav a.reports").removeClass("disabled");
  }

  if (
    usrInfo.hasOwnProperty("AdvertiserSurvey") &&
    usrInfo.AdvertiserSurvey.toLowerCase() == "true"
  ) {
    $(".nav a.survey").removeClass("disabled");
  }

  if (
    usrInfo.hasOwnProperty("UsAdForecast") &&
    usrInfo.UsAdForecast.toLowerCase() == "true"
  ) {
    $(".nav a.ad-forecast").removeClass("disabled");
  }
}

function setup() {
  getApiVersion().done(function(data) {
    var version =
      "Version: " +
      +data.major +
      "." +
      data.majorRevision +
      "." +
      data.minor +
      "." +
      data.minorRevision;
    $(".login-version").text(version);
    $(".footer-version").text(" / " + version);
  });
}

function getApiVersion() {
  return getJson("https://advantage.bia.com/api/Version").then(function(
    result
  ) {
    return result;
  });
}

function getAvailableYears() {
  return getJson(
    "https://advantage.bia.com/api/MediaAdView/AvailableYears"
  ).then(function(result) {
    return result;
  });
}

function setupPrint(button, chart, tooltip, chartType) {
  button.dxButton({
    icon: "print",
    height: "34px",
    width: "34px",
    onClick: function() {
      var chartInstance;
      if (chartType == "bar") {
        chartInstance = chart.dxChart("instance");
      } else if (chartType == "table") {
        chartInstance = chart.dxDataGrid("instance");
      } else {
        chartInstance = chart.dxPieChart("instance");
      }
      chartInstance.option("title", {
        text:
          chart
            .prev()
            .find(".year")
            .text() +
          " " +
          chart
            .parents()
            .eq(2)
            .find(".title")
            .text(),
        subtitle: {
          text: chart
            .parents()
            .eq(2)
            .find(".mkt-title")
            .text(),
          font: {
            size: 12
          }
        },
        font: {
          size: 16
        },
        margin: {
          top: 0
        }
      });
      chart.prepend(
        '<img class="print-logo" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/532833/advantageheader.png">'
      );
      chart.printMe({
        path: [
          "https://cdn3.devexpress.com/jslib/17.2.4/css/dx.common.css",
          "https://cdn3.devexpress.com/jslib/17.2.4/css/dx.light.css",
          "css/style.css",
          "css/print.css"
        ]
      });
      chartInstance.option("title", { text: "", subtitle: "" });
      $(".print-logo").remove();
    }
  });
  tooltip.removeClass("hidden");
  tooltip.dxTooltip({
    target: button,
    showEvent: "dxhoverstart",
    hideEvent: "dxhoverend",
    closeOnBackButton: false,
    closeOnOutsideClick: false
  });
}

function setupComparePrint(button, chart1, chart2, tooltip) {
  button.dxButton({
    icon: "print",
    height: "34px",
    width: "34px",
    onClick: function() {
      var chartInstance1 = chart1.dxPieChart("instance"),
        chartInstance2 = chart2.dxPieChart("instance");
      chartInstance1.option("title", {
        text:
          chart1
            .prev()
            .find(".year")
            .text() +
          " " +
          chart1
            .parents()
            .eq(1)
            .find(".title")
            .text(),
        subtitle: {
          text: chart1
            .parents()
            .eq(1)
            .find(".mkt-title")
            .text(),
          font: {
            size: 12
          }
        },
        font: {
          size: 16
        },
        margin: {
          top: 0
        }
      });
      chartInstance2.option("title", {
        text:
          chart2
            .prev()
            .find(".year")
            .text() +
          " " +
          chart2
            .parents()
            .eq(1)
            .find(".title")
            .text(),
        subtitle: {
          text: chart2
            .parents()
            .eq(1)
            .find(".mkt-title")
            .text(),
          font: {
            size: 12
          }
        },
        font: {
          size: 16
        },
        margin: {
          top: 0
        }
      });
      chart1.prepend(
        '<img class="print-logo" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/532833/advantageheader.png">'
      );
      chart1
        .parents()
        .eq(3)
        .printMe({ path: ["css/style.css", "css/print.css"] });
      $(".print-logo").remove();
      chartInstance1.option("title", { text: "", subtitle: "" });
      chartInstance2.option("title", { text: "", subtitle: "" });
    }
  });
  tooltip.removeClass("hidden");
  tooltip.dxTooltip({
    target: button,
    showEvent: "dxhoverstart",
    hideEvent: "dxhoverend",
    closeOnBackButton: false,
    closeOnOutsideClick: false
  });
}

function setupDGPrint(button, chart, tooltip) {
  button.dxButton({
    icon: "print",
    height: "34px",
    width: "34px",
    onClick: function() {
      var chartInstance = chart.dxDataGrid("instance");
      chartInstance.printReport();
    }
  });

  tooltip.removeClass("hidden");

  tooltip.dxPopover({
    target: button,
    showEvent: "mouseenter",
    hideEvent: "mouseleave",
    position: "top",
    width: "145px",
    animation: {
      show: {
        type: "pop",
        from: { scale: 0 },
        to: { scale: 1 }
      },
      hide: {
        type: "fade",
        from: 1,
        to: 0
      }
    }
  });
}

function setupExport(button, chart, tooltip, chartType) {
  button.dxButton({
    icon: "export",
    height: "34px",
    width: "34px",
    onClick: function() {
      var chartInstance;
      if (chartType == "bar") {
        chartInstance = chart.dxChart("instance");
      } else if (chartType == "table") {
        chartInstance = chart.dxDataGrid("instance");
      } else {
        chartInstance = chart.dxPieChart("instance");
      }
      chartInstance.option("title", {
        text:
          chart
            .prev()
            .find(".year")
            .text() +
          " " +
          chart
            .parents()
            .eq(2)
            .find(".title")
            .text(),
        subtitle: {
          text: chart
            .parents()
            .eq(2)
            .find(".mkt-title")
            .text(),
          font: {
            size: 12
          }
        },
        font: {
          size: 16
        },
        margin: {
          top: 0
        }
      });
      chart.prepend('<img class="print-logo" src="img/advantageheader.png">');

      html2canvas(chart, {
        allowTaint: false,
        useCORS: true,
        taintTest: true,
        background: "#fff",
        width: 700,
        height: 550,
        onrendered: function(canvas) {
          var exportCanvas = canvas.toDataURL("image/png"),
            renderCanvas = exportCanvas.replace(
              /^data:image\/png/,
              "data:application/octet-stream"
            );
          button
            .next("a")
            .attr("download", "screenshot.png")
            .attr("href", renderCanvas);
          button.next("a")[0].click();
        }
      });

      chartInstance.option("title", { text: "", subtitle: "" });
      $(".print-logo").remove();
    }
  });

  tooltip.removeClass("hidden");

  tooltip.dxTooltip({
    target: button,
    showEvent: "dxhoverstart",
    hideEvent: "dxhoverend",
    closeOnBackButton: false,
    closeOnOutsideClick: false
  });
}

function setupCompareExport(button, chart1, chart2, tooltip) {
  button.dxButton({
    icon: "export",
    height: "34px",
    width: "34px",
    onClick: function() {
      var chartInstance1 = chart1.dxPieChart("instance"),
        chartInstance2 = chart2.dxPieChart("instance");
      chartInstance1.option("title", {
        text:
          chart1
            .prev()
            .find(".year")
            .text() +
          " " +
          chart1
            .parents()
            .eq(1)
            .find(".title")
            .text(),
        subtitle: {
          text: chart1
            .parents()
            .eq(1)
            .find(".category")
            .text(),
          font: {
            size: 12
          }
        },
        font: {
          size: 16
        },
        margin: {
          top: 0
        }
      });
      chartInstance2.option("title", {
        text:
          chart2
            .prev()
            .find(".year")
            .text() +
          " " +
          chart2
            .parents()
            .eq(1)
            .find(".title")
            .text(),
        subtitle: {
          text: chart2
            .parents()
            .eq(1)
            .find(".category")
            .text(),
          font: {
            size: 12
          }
        },
        font: {
          size: 16
        },
        margin: {
          top: 0
        }
      });

      var charts = chart1.parents().eq(3);
      charts.prepend(
        '<img class="print-logo compare" src="img/advantageheader.png">'
      );
      charts.find(".title, .category, nav, .footer").hide();
      html2canvas(charts, {
        allowTaint: false,
        useCORS: true,
        taintTest: true,
        background: "#fff",
        width: 1500,
        height: 600,
        onrendered: function(canvas) {
          var exportCanvas = canvas.toDataURL("image/png"),
            renderCanvas = exportCanvas.replace(
              /^data:image\/png/,
              "data:application/octet-stream"
            );
          button
            .next("a")
            .attr("download", "screenshot.png")
            .attr("href", renderCanvas);
          button.next("a")[0].click();
        }
      });
      chartInstance1.option("title", { text: "", subtitle: "" });
      chartInstance2.option("title", { text: "", subtitle: "" });
      $(".print-logo").remove();
      charts.find(".title, .category, nav, .footer").show();
    }
  });
  tooltip.removeClass("hidden");
  tooltip.dxTooltip({
    target: button,
    showEvent: "dxhoverstart",
    hideEvent: "dxhoverend",
    closeOnBackButton: false,
    closeOnOutsideClick: false
  });
}

function setupDGExport(button, chart, tooltip) {
  button.dxButton({
    icon: "export",
    height: "34px",
    width: "34px",
    onClick: function() {
      var chartInstance = chart.dxDataGrid("instance");
      chartInstance.exportAll("Example.xlsx");
    }
  });

  tooltip.removeClass("hidden");

  tooltip.dxTooltip({
    target: button,
    showEvent: "dxhoverstart",
    hideEvent: "dxhoverend",
    closeOnBackButton: false,
    closeOnOutsideClick: false
  });
}

function setupAdSpendTrigger(
  button,
  tooltip,
  text = "Compare National",
  strIcon = "fa fa-pie-chart"
) {
  button.dxButton({
    icon: strIcon,
    text: text,
    height: "34px",
    width: "auto",
    onClick: function() {
      $(
        ".card.expand.adSpendCompare ,.adSpendCompare .header.expand ,.adSpendCompare .content.expand"
      ).toggleClass("expanded");
    }
  });

  tooltip.removeClass("hidden");

  tooltip.dxTooltip({
    target: button,
    showEvent: "dxhoverstart",
    hideEvent: "dxhoverend",
    closeOnBackButton: false,
    closeOnOutsideClick: false
  });
}

function setupDigitalSpendTrigger(
  button,
  tooltip,
  text = "Compare National",
  strIcon = "fa fa-pie-chart"
) {
  button.dxButton({
    icon: strIcon,
    text: text,
    height: "34px",
    width: "auto",
    onClick: function() {
      $(
        ".card.expand.digSpendCompare ,.digSpendCompare .header.expand ,.digSpendCompare .content.expand"
      ).toggleClass("expanded");

      $(".table-button").toggleClass("disabled");
    }
  });

  tooltip.removeClass("hidden");

  tooltip.dxTooltip({
    target: button,
    showEvent: "dxhoverstart",
    hideEvent: "dxhoverend",
    closeOnBackButton: false,
    closeOnOutsideClick: false
  });
}

function setupTableTrigger(
  button,
  tooltip,
  text = "Table View",
  strIcon = "fa fa-table"
) {
  button.dxButton({
    icon: strIcon,
    text: text,
    height: "34px",
    width: "auto",
    onClick: function() {
      $(
        ".card.expand.locForcast ,.locForcast .header.expand ,.locForcast .content.expand"
      ).toggleClass("expanded");
      $(".table-button").toggleClass("clicked");
    }
  });

  tooltip.removeClass("hidden");

  tooltip.dxTooltip({
    target: button,
    showEvent: "dxhoverstart",
    hideEvent: "dxhoverend",
    closeOnBackButton: false,
    closeOnOutsideClick: false
  });
}

function showLoadPanel(panel, container, shading, strText = "Loading...") {
  panel.dxLoadPanel({
    message: strText,
    visible: true,
    shading: shading,
    shadingColor: "rgba(0,0,0,0.4)",
    showPane: true
  });

  panel.show();

  return panel;
}

function hideLoadPanel(panel) {
  var loadPanel = panel.dxLoadPanel("instance");
  if (loadPanel != null) {
    loadPanel.hide();
  }
}

// sort by / then by json array
var by = function(path, reverse, primer, then) {
  var get = function(obj, path) {
      if (path) {
        path = path.split(".");
        for (var i = 0, len = path.length - 1; i < len; i++) {
          obj = obj[path[i]];
        }
        return obj[path[len]];
      }
      return obj;
    },
    prime = function(obj) {
      return primer ? primer(get(obj, path)) : get(obj, path);
    };

  return function(a, b) {
    var A = prime(a),
      B = prime(b);

    return (
      (A < B ? -1 : A > B ? 1 : typeof then === "function" ? then(a, b) : 0) *
      [1, -1][+!!reverse]
    );
  };
};
