$(document).on("navSelected", "a.radio.selected", function() {
  //Chart Config
  var $market = 1,
    $mktRank = $(".mkt-rank"),
    $initYear = "2018",
    $endYear = "2022",
    $pallet = "Soft Pastel",
    $legendShow = true,
    $tooltipShow = true,
    $exportShow = false,
    $labelOverlap = "shift",
    $adaptLayout = 300,
    $tooltipType = "currency",
    $tooltipPrec = 0,
    $tooltipCurr = "USD",
    $animation = { enabled: "easeOutCubic", duration: 500 },
    $series = [
      {
        argumentField: "name",
        valueField: "total",
        border: {
          visible: "true",
          width: 0.5
        },
        label: {
          visible: true,
          displayMode: "stagger",
          staggeringSpacing: 20,
          position: "columns",
          font: {
            size: 12
          },
          connector: {
            visible: true,
            width: 1
          },
          border: {
            visible: true,
            color: "rgba(0, 0, 0, 0.25)",
            width: 1
          },
          format: "fixedPoint",
          percentPrecision: 1,
          customizeText: function(pnt) {
            var series = pnt.point.series.getAllPoints();
            series.sort(
              (a, b) => Number(b.percent * 100) - Number(a.percent * 100)
            );

            var pntIndex = series.findIndex(x => x.argument == pnt.argument);

            // only display the top 8
            // counting on the sort order to max -> min
            if (pntIndex < 5) {
              return "<b>" + pnt.percentText + "</b><br>" + pnt.argumentText;
            }

            return "";
          }
        },
        minSegmentSize: 5,
        onLegendClick: function(info) {
          var pieChart = info.component;
          var argument = info.target;
          pieChart
            .getAllSeries()[0]
            .getPointsByArg(argument)[0]
            .showTooltip();
        }
      }
    ];

  loadRadioContent();

  function loadRadioContent() {
    $(".select-item.market").on("change.radio", function() {
      if ($(".radio").hasClass("selected")) {
        loadRadioContent();
      } else {
        $(".select-item.market").off("change.radio");
      }
    });

    $market = getSelectedMarket().val();

    if ($market != "") {
      var $token = authToken(),
        $mktSelected = getSelectedMarket().text();

      $mktRank.text(getSelectedMarket().data("rank"));

      // disable any markets that don't have a radio station
      // jacked up but this is the way the semantic dropdown
      // seems to handle the disabled items
      $(".filters-market option[data-radio-market='false']").each(function() {
        var id = $(this).val();
        $(".filters-market .item[data-value='" + id + "']").addClass(
          "disabled"
        );
      });

      //refresh session
      $token ? setAuthToken($token) : location.reload();

      mktTitle($mktSelected);
      function mktTitle() {
        if ($(".radio .mkt-title").length < 2) {
          window.requestAnimationFrame(mktTitle);
        } else {
          $(".radio .mkt-title, .navbar-brand .mkt-title").text($mktSelected);
        }
      }

      showLoadPanel($("#loadPanel"), $(".cd-main"), true);

      $(".select-item.subvert").removeClass("enable");
      $(".select-item.market.main-market, .navbar-brand").addClass("enable");

      //Get TV Market Comp
      var radioComp = [],
        radioCompUrl =
          "https://advantage.bia.com/api/MediaAdView/GetOTARevenues/" + $market;
      $.ajax({
        url: radioCompUrl,
        async: true,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        headers: { "x-api-version": "2.0" },
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Bearer " + $token);
        },
        success: function(radioCompData) {
          if (radioCompData) {
            var radioCompTotal = radioCompData.radioOTATotal,
              $radioCompTotal = $(".radioComp-total");
            $radioCompTotal.text(
              "$" +
                radioCompTotal
                  .toFixed(0)
                  .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +
                " (000)s"
            );
            $.each(radioCompData.topRadioOwners, function(index, value) {
              radioComp.push({
                name: value.name,
                total: value.revenue
              });
            });

            $("#radioComp").dxPieChart({
              palette: $pallet,
              dataSource: radioComp,
              legend: {
                visible: true,
                rowCount: 3,
                verticalAlignment: "bottom",
                horizontalAlignment: "center",
                itemTextPosition: "right",
                customizeText: function() {
                  var pieChart = $("#radioComp").dxPieChart("instance"),
                    percentValue = (
                      pieChart.getAllSeries()[0].getAllPoints()[this.pointIndex]
                        .percent * 100
                    ).toFixed(1);
                  return this.pointName + " (" + percentValue + "%)";
                }
              },
              tooltip: {
                enabled: $tooltipShow,
                format: {
                  type: $tooltipType,
                  precision: $tooltipPrec,
                  currency: $tooltipCurr
                },
                customizeTooltip: function(args) {
                  return {
                    html:
                      "<div><h5>" +
                      args.argumentText +
                      "</h5></div>" +
                      "<div><b>Amount: </b>" +
                      args.valueText +
                      "</div>" +
                      "<div><b>Percentage: </b>" +
                      (args.percent * 100).toFixed(1) +
                      " %</div>"
                  };
                }
              },
              export: {
                enabled: $exportShow
              },
              resolveLabelOverlapping: $labelOverlap,
              adaptiveLayout: {
                width: $adaptLayout
              },
              animation: $animation,
              series: $series
            });

            setupPrint(
              $("#radioCompPrint"),
              $("#radioComp"),
              $("#radioCompPrintTooltip")
            );
            setupExport(
              $("#radioCompExport"),
              $("#radioComp"),
              $("#radioCompExportTooltip")
            );
          }

          hideLoadPanel($("#loadPanel"));
        },
        error: function(err) {
          console.log("Error: Cannot Reach API");
        }
      });

      //Update Year Selection
      $(".radioComp .button.prev").unbind();
      $(".radioComp .button.prev").on("click", function() {
        $(".radioComp .button.next").removeClass("disabled");
        var $year = $(".radioComp .year").text(),
          $yearNum = parseInt($year);
        if ($yearNum != $initYear) {
          $(".radioComp .year a").text($yearNum - 1);
        }
        if ($yearNum == $initYear + 1) {
          $(".radioComp .button.prev").addClass("disabled");
        }
        var $radioCompYr = $(".radioComp .year").text(),
          radioComp = [],
          radioCompUrl =
            "https://advantage.bia.com/api/MediaAdView/GetOTARevenues/" +
            $market;
        $.ajax({
          url: radioCompUrl,
          async: true,
          type: "GET",
          dataType: "json",
          contentType: "application/json",
          headers: { "x-api-version": "2.0" },
          beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + $token);
          },
          success: function(radioCompData) {
            if (radioCompData) {
              console.log(radioCompData);
              var radioCompTotal = radioCompData.radioOTATotal,
                $radioCompTotal = $(".radioComp-total");
              $radioCompTotal.text(
                "$" +
                  radioCompTotal
                    .toFixed(0)
                    .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +
                  " (000)s"
              );
              $.each(radioCompData.topRadioOwners, function(index, value) {
                radioComp.push({
                  name: value.name,
                  total: value.revenue
                });
              });
              $("#radioComp").dxPieChart("option", "dataSource", radioComp);
            }
          },
          error: function(err) {
            console.log("Error: Cannot Reach API");
          }
        });
      });

      $(".radioComp .button.next").unbind();
      $(".radioComp .button.next").on("click", function() {
        $(".radioComp .button.prev").removeClass("disabled");
        var $year = $(".radioComp .year").text(),
          $yearNum = parseInt($year);
        if ($yearNum != $endYear) {
          $(".radioComp .year a").text($yearNum + 1);
        }
        if ($yearNum == $endYear - 1) {
          $(".radioComp .button.next").addClass("disabled");
        }
        var $radioCompYr = $(".radioComp .year").text(),
          radioComp = [],
          radioCompUrl =
            "https://advantage.bia.com/api/MediaAdView/GetOTARevenues/" +
            $market;
        $.ajax({
          url: radioCompUrl,
          async: true,
          type: "GET",
          dataType: "json",
          contentType: "application/json",
          headers: { "x-api-version": "2.0" },
          beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + $token);
          },
          success: function(radioCompData) {
            if (radioCompData) {
              console.log(radioCompData);
              var radioCompTotal = radioCompData.radioOTATotal,
                $radioCompTotal = $(".radioComp-total");
              $radioCompTotal.text(
                "$" +
                  radioCompTotal
                    .toFixed(0)
                    .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +
                  " (000)s"
              );
              $.each(radioCompData.topRadioOwners, function(index, value) {
                radioComp.push({
                  name: value.name,
                  total: value.revenue
                });
              });
              $("#radioComp").dxPieChart("option", "dataSource", radioComp);
            }
          },
          error: function(err) {
            console.log("Error: Cannot Reach API");
          }
        });
      });

      //Get Radio Market Revenues
      var radioRevenue = [],
        radioRevenueUrl =
          "https://advantage.bia.com/api/MediaAdView/GetOTARevenues/" + $market;
      $.ajax({
        url: radioRevenueUrl,
        async: true,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        headers: { "x-api-version": "2.0" },
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Bearer " + $token);
        },
        success: function(radioRevenueData) {
          if (radioRevenueData) {
            $.each(radioRevenueData.radioEstimatedRevenues, function(
              index,
              value
            ) {
              radioRevenue.push({
                name: String(value.year),
                ota: value.ota,
                online: value.online
              });
            });

            $("#radioRevenue").dxChart({
              dataSource: radioRevenue,
              commonAxisSettings: {
                maxValueMargin: 0.1
              },
              commonSeriesSettings: {
                argumentField: "name",
                type: "bar",
                hoverMode: "allArgumentPoints",
                selectionMode: "allArgumentPoints",
                label: {
                  visible: true,
                  format: {
                    type: "fixedPoint",
                    precision: 0
                  }
                }
              },
              series: [
                { valueField: "ota", name: "Over-The-Air" },
                { valueField: "online", name: "Online" }
              ],
              title: "",
              legend: {
                verticalAlignment: "top",
                horizontalAlignment: "center"
              },
              valueAxis: {
                title: {
                  text: "millions"
                },
                label: {
                  font: {
                    size: 10
                  },
                  customizeText: function() {
                    return this.value / 1000;
                  },
                  showForZeroValues: false
                },
                position: "left"
              },
              export: {
                enabled: $exportShow
              },
              onPointClick: function(e) {
                e.target.select();
              }
            });
            setupPrint(
              $("#radioRevenuePrint"),
              $("#radioRevenue"),
              $("#radioRevenuePrintTooltip")
            );
            setupExport(
              $("#radioRevenueExport"),
              $("#radioRevenue"),
              $("#radioRevenueExportTooltip")
            );
          }

          setTimeout(hideLoadPanel($("#loadPanel")), 1000);
        },
        error: function(err) {
          console.log("Error: Cannot Reach API");
        }
      });

      // we have to delay for the page to render before we set the
      // source in the iframe
      setTimeout(function() {
        var PDFUrl =
          "https://advantage.bia.com/api/MediaAdView/GetRadioInvestingInPDF/" +
          $market;

        $("#pdfViewer").attr(
          "src",
          "pdfview/viewer.html?file=" + PDFUrl + "#page=2"
        );
      }, 800);
    }
  }
});
