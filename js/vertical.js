$(document).on("navSelected", "a.selected", function() {
  if (
    $(this).data("menu") != "subvertical" &&
    $(this).data("menu") != "vertical" &&
    !$(this).hasClass("vertical")
  ) {
    console.log("Not a vertical or sub-vertical");
    return;
  }

  //Chart Config
  var $market = 1,
    $mktRank = $(".mkt-rank"),
    $initYear = "2018",
    $endYear = "2022",
    $pallet = "Soft Pastel",
    $legendShow = true,
    $tooltipShow = true,
    $exportShow = false,
    $labelOverlap = "shift",
    $adaptLayout = 100,
    $tooltipType = "currency",
    $tooltipPrec = 0,
    $tooltipCurr = "USD",
    $animation = {
      enabled: "easeOutCubic",
      duration: 500
    },
    $series = [
      {
        argumentField: "category",
        valueField: "marketTotal",
        border: {
          visible: "true",
          width: 0.5
        },
        label: {
          visible: true,
          displayMode: "stagger",
          staggeringSpacing: 20,
          position: "columns",
          font: {
            size: 12
          },
          connector: {
            visible: true,
            width: 1
          },
          border: {
            visible: true,
            color: "rgba(0, 0, 0, 0.25)",
            width: 1
          },
          format: "fixedPoint",
          percentPrecision: 1,
          customizeText: function(pnt) {
            var series = pnt.point.series.getAllPoints();
            series.sort(
              (a, b) => Number(b.percent * 100) - Number(a.percent * 100)
            );

            var pntIndex = series.findIndex(x => x.argument == pnt.argument);

            // only display the top 8
            // counting on the sort order to max -> min
            if (pntIndex < 8) {
              return "<b>" + pnt.percentText + "</b><br>" + pnt.argumentText;
            }

            return "";
          }
        },
        minSegmentSize: 4
      }
    ];

  loadVerticalContent();

  function loadVerticalContent() {
    $(".select-item.market").on("change.vertical", function() {
      if (
        $("#nav")
          .find('a[data-menu="vertical"]')
          .hasClass("selected") ||
        $(".vertical").hasClass("selected")
      ) {
        loadVerticalContent();
      } else {
        $(".select-item.market").off("change.vertical");
      }
    });

    if (
      $("#nav")
        .find('a[data-menu="vertical"]')
        .hasClass("selected") ||
      $(".vertical").hasClass("selected")
    ) {
      $market = getSelectedMarket().val();
      if ($market != "") {
        var $token = authToken(),
          $mktSelected = getSelectedMarket().text();

        $mktRank.text(getSelectedMarket().data("rank"));

        //reresh session
        $token ? setAuthToken($token) : location.reload();

        mktTitle($mktSelected);
        function mktTitle() {
          if ($(".vertical .mkt-title").length < 2) {
            window.requestAnimationFrame(mktTitle);
          } else {
            $(".vertical .mkt-title, .navbar-brand .mkt-title").text(
              $mktSelected
            );
          }
        }

        $(".year a").text($initYear);
        $(".button.prev").addClass("disabled");

        showLoadPanel($("#loadPanel"), $(".cd-main"), false);

        $(".select-item.subvert").addClass("enable");
        $(".select-item.market.main-market, .navbar-brand").addClass("enable");

        $(".nav a")
          .not(
            $(".nav a.radio"),
            $(".nav a.mapro"),
            $(".nav a.survey"),
            $(".nav a.reports"),
            $(".nav a.ad-forecast")
          )
          .removeClass("disabled");

        var marketObj = JSON.parse(atob(getSelectedMarket().attr("data-obj")));
        // get the market json object returned from the API
        if (!$.isEmptyObject(marketObj) && marketObj.marketRadioNbr == 0) {
          // if they don't have a radio market then
          // let's disable the radio OTA menu option
          $(".nav a.radio").addClass("disabled");
        } else {
          $(".nav a.radio").removeClass("disabled");
        }

        var $vertName = $("#nav")
            .find("a.selected")
            .text(),
          $vN = $vertName.slice(0, 4),
          $vertId = $("#nav .selected").attr("data-id");
        vertName();

        function vertName() {
          if ($(".vert-name").length < 2) {
            window.requestAnimationFrame(vertName);
          } else {
            $vertName = $("#nav")
              .find("a.selected")
              .text();
            $(".vert-name").text($initYear + " " + $vertName);
          }
        }

        var $subVertical = $(".main-subvert .filters-subvert").dropdown(
            "get value"
          ),
          subvUrl = "https://advantage.bia.com/api/SubVerticals";
        $.ajax({
          url: subvUrl,
          async: true,
          type: "GET",
          dataType: "json",
          contentType: "application/json",
          headers: {
            "x-api-version": "2.0"
          },
          beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + $token);
          },
          success: function(subvdata) {
            if (
              subvdata.length > 0 &&
              $(".filters-subvert .menu").hasClass("is-vertical")
            ) {
              $.each(subvdata, function(index, value) {
                if (value.verticalId == $vertId) {
                  $(".filters-subvert .menu").append(
                    '<div class="item" data-menu="subvertical" data-value="' +
                      value.id +
                      '">' +
                      value.name +
                      "</div>"
                  );
                }
              });
            }
            subvertSelect(
              subvdata,
              $vertName,
              $mktSelected,
              $subVertical,
              $token
            );

            hideLoadPanel($("#loadPanel"));
          },
          error: function(err) {
            console.log("Error: Cannot Reach API");
          }
        });
      } else {
        $(".select-item.subvert").removeClass("enable");
      }
    }
    if (
      $("#nav")
        .find('a[data-menu="vertical"]')
        .hasClass("selected") &&
      $market != ""
    ) {
      //Get Vertical Ad Spend
      var vertSpend = [],
        vertSpendUrl =
          "https://advantage.bia.com/api/MediaAdView/GetVerticalOverviewByMarket/" +
          $market +
          "/" +
          $vertId +
          "/" +
          $initYear +
          "/" +
          $initYear;
      $.ajax({
        url: vertSpendUrl,
        async: true,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        headers: {
          "x-api-version": "2.0"
        },
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Bearer " + $token);
        },
        success: function(vertSpendData) {
          if (vertSpendData) {
            var vertSpendRevenues =
                vertSpendData.estimatedAdvertisingRevenues[0],
              vertSpendTotal = vertSpendRevenues.total,
              $vertSpendTotal = $(".vertSpend-total");
            $vertSpendTotal.text(
              "$" +
                vertSpendTotal
                  .toFixed(0)
                  .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +
                " (000)s"
            );
            $.each(vertSpendRevenues.mediaTotals, function(index, value) {
              vertSpend.push({
                category: value.name,
                marketTotal: value.value
              });
            });

            $("#vertSpend").dxPieChart({
              palette: $pallet,
              dataSource: vertSpend,
              legend: {
                visible: true,
                rowCount: 3,
                verticalAlignment: "bottom",
                horizontalAlignment: "center",
                itemTextPosition: "right",
                customizeText: function() {
                  var pieChart = $("#vertSpend").dxPieChart("instance"),
                    percentValue = (
                      pieChart.getAllSeries()[0].getAllPoints()[this.pointIndex]
                        .percent * 100
                    ).toFixed(1);
                  return this.pointName + " (" + percentValue + "%)";
                }
              },
              tooltip: {
                enabled: $tooltipShow,
                format: {
                  type: $tooltipType,
                  precision: $tooltipPrec,
                  currency: $tooltipCurr
                },
                customizeTooltip: function(args) {
                  return {
                    html:
                      "<div><h5>" +
                      args.argumentText +
                      "</h5></div>" +
                      "<div><b>Amount: </b>" +
                      args.valueText +
                      "</div>" +
                      "<div><b>Percentage: </b>" +
                      (args.percent * 100).toFixed(1) +
                      " %</div>"
                  };
                }
              },
              export: {
                enabled: $exportShow
              },
              resolveLabelOverlapping: $labelOverlap,
              adaptiveLayout: {
                width: $adaptLayout
              },
              animation: $animation,
              series: $series,
              onLegendClick: function(info) {
                var pieChart = info.component;
                var argument = info.target;
                pieChart
                  .getAllSeries()[0]
                  .getPointsByArg(argument)[0]
                  .showTooltip();
              }
            });

            setupPrint(
              $("#vertSpendPrint"),
              $("#vertSpend"),
              $("#vertSpendPrintTooltip")
            );
            setupExport(
              $("#vertSpendExport"),
              $("#vertSpend"),
              $("#vertSpendExportTooltip")
            );
          }
          hideLoadPanel($("#loadPanel"));
        },
        error: function(err) {
          console.log("Error: Cannot Reach API");
        }
      });

      //Get Vertical Breakdown
      var vertBreakUrl =
          "https://advantage.bia.com/api/MediaAdView/GetVerticalOverviewByMarket/" +
          $market +
          "/" +
          $vertId +
          "/" +
          $initYear +
          "/" +
          $initYear,
        vertBreak = [];
      $.ajax({
        url: vertBreakUrl,
        async: true,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        headers: {
          "x-api-version": "2.0"
        },
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Bearer " + $token);
        },
        success: function(vertdata) {
          if (vertdata) {
            $.each(vertdata.subVerticalAdSpendingByVertical, function(
              index,
              value
            ) {
              vertBreak.push({
                name: value.name,
                total: value.total
              });
            });
            $("#vertBreakdown").dxDataGrid({
              dataSource: vertBreak,
              export: {
                enabled: $exportShow,
                fileName: $vertName + " Vertical Breakdown",
                allowExportSelectedData: false
              },
              onCellPrepared: function(options) {
                if (options.value) {
                  if (options.column.dataField == "change") {
                    if (options.value < 0) {
                      options.cellElement.addClass("negative");
                    } else {
                      options.cellElement.addClass("positive");
                    }
                  }
                }
              },
              summary: {
                totalItems: [
                  {
                    column: "total",
                    summaryType: "sum",
                    customizeText: function(data) {
                      return (
                        "$" +
                        data.value
                          .toFixed(0)
                          .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
                      );
                    }
                  }
                ]
              },
              columns: [
                {
                  dataField: "name",
                  caption: "Vertical",
                  sortOrder: "asc"
                },
                {
                  dataField: "total",
                  caption: $initYear + " Media Ad Spending",
                  width: 175,
                  format: "currency"
                }
              ],
              paging: {
                pageSize: 7
              }
            });
          }
        },
        error: function(err) {
          console.log("Error: Cannot Reach API");
          if (err.status == "400") {
            $("#vertBreakdown").html(
              "<span>No Data Exists for this Vertical and Market</span>"
            );
          }
        }
      });

      //Get Media Ad Spending
      var vertUrl =
          "https://advantage.bia.com/api/MediaAdView/GetVerticalOverviewByMarket/" +
          $market +
          "/" +
          $vertId +
          "/" +
          ($initYear - 1) +
          "/" +
          $initYear,
        vertSpendTbl = [],
        cvertSpend = [];
      $.ajax({
        url: vertUrl,
        async: true,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        headers: {
          "x-api-version": "2.0"
        },
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Bearer " + $token);
        },
        success: function(vertdata) {
          if (vertdata) {
            var pvertSpendRevenues = vertdata.estimatedAdvertisingRevenues[0],
              cvertSpendRevenues = vertdata.estimatedAdvertisingRevenues[1];
            $.each(pvertSpendRevenues.mediaTotals, function(index, value) {
              vertSpendTbl.push({
                category: value.name,
                pTotal: value.value
              });
            });
            $.each(cvertSpendRevenues.mediaTotals, function(index, value) {
              cvertSpend.push({
                cTotal: value.value,
                percent: value.percentage,
                change: value.percentChange
              });
            });
            $.extend(true, vertSpendTbl, cvertSpend);
          }

          $("#vertTable").dxDataGrid({
            dataSource: vertSpendTbl,
            export: {
              enabled: true,
              fileName: $initYear + " Vertical Ad Spending",
              allowExportSelectedData: false
            },
            onCellPrepared: function(options) {
              if (options.value) {
                if (options.column.dataField == "change") {
                  if (options.value < 0) {
                    options.cellElement.addClass("negative");
                  } else {
                    options.cellElement.addClass("positive");
                  }
                }
              }
            },
            summary: {
              totalItems: [
                {
                  column: "pTotal",
                  summaryType: "sum",
                  customizeText: function(data) {
                    return (
                      "$" +
                      data.value
                        .toFixed(0)
                        .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
                    );
                  }
                },
                {
                  column: "cTotal",
                  summaryType: "sum",
                  customizeText: function(data) {
                    return (
                      "$" +
                      data.value
                        .toFixed(0)
                        .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
                    );
                  }
                }
              ]
            },
            columns: [
              {
                dataField: "category",
                caption: "Media Type",
                sortOrder: "asc"
              },
              {
                dataField: "pTotal",
                caption: $initYear - 1 + " Local Advertising",
                format: "currency"
              },
              {
                dataField: "cTotal",
                caption: $initYear + " Local Advertising",
                format: "currency"
              },
              {
                dataField: "percent",
                caption: "Percent of Total",
                format: {
                  type: "percent",
                  precision: 1
                }
              },
              {
                dataField: "change",
                caption: "Annual Change",
                format: {
                  type: "percent",
                  precision: 1
                }
              }
            ]
          });
          setupPrint(
            $("#mediaAdSpendPrint"),
            $("#vertTable"),
            $("#printMediaAdSpendTooltip"),
            "table"
          );
        },
        error: function(err) {
          console.log("Error: Cannot Reach API");
        }
      });
    }
  }

  function subvertSelect(
    $subvdata,
    $vertName,
    $mktSelected,
    $subVertical,
    $token
  ) {
    //    var $subVertical = $(".main-subvert .filters-subvert").dropdown(
    //      "get value"
    //    );
    if ($subvdata && $subVertical != "") {
      console.log("Sub-Vertical Selected");

      showLoadPanel($("#loadPanel"), $(".cd-main"), false);

      $.each($subvdata, function(index, value) {
        if (value.id == $subVertical) {
          $(".vert-name").text($vertName + " / ");
          $(".subvert-name").text(value.name);
          $(".subvert-name-yr").text($initYear + " " + value.name);
          $(".subvert-desc").text(value.description);
          mktTitle($mktSelected);

          function mktTitle() {
            if ($(".subvertical .mkt-title").length < 2) {
              window.requestAnimationFrame(mktTitle);
            } else {
              $(".subvertical .mkt-title, .navbar-brand .mkt-title").text(
                $mktSelected
              );
            }
          }
          //Get Vertical Ad Spend
          var subvertSpend = [],
            subvertSpendUrl =
              "https://advantage.bia.com/api/MediaAdView/GetAnnualAdRevenueGrowth/" +
              $market +
              "/" +
              $subVertical +
              "/" +
              ($initYear - 1) +
              "/" +
              $initYear;
          $.ajax({
            url: subvertSpendUrl,
            async: true,
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            headers: {
              "x-api-version": "2.0"
            },
            beforeSend: function(xhr) {
              xhr.setRequestHeader("Authorization", "Bearer " + $token);
            },
            success: function(subvertSpendData) {
              if (subvertSpendData) {
                var subvertSpendRevenues =
                    subvertSpendData.verticalEstimatedRevenues[1],
                  subvertSpendTotal = subvertSpendRevenues.total,
                  $subvertSpendTotal = $(".subvertSpend-total");
                $subvertSpendTotal.text(
                  "$" +
                    subvertSpendTotal
                      .toFixed(0)
                      .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +
                    " (000)s"
                );
                $.each(subvertSpendRevenues.mediaTotals, function(
                  index,
                  value
                ) {
                  subvertSpend.push({
                    category: value.name,
                    marketTotal: value.value
                  });
                });

                $("#subvertSpend").dxPieChart({
                  palette: $pallet,
                  dataSource: subvertSpend,
                  legend: {
                    visible: $legendShow,
                    rowCount: 3,
                    verticalAlignment: "bottom",
                    horizontalAlignment: "center",
                    itemTextPosition: "right",
                    customizeText: function() {
                      var pieChart = $("#subvertSpend").dxPieChart("instance"),
                        percentValue = (
                          pieChart.getAllSeries()[0].getAllPoints()[
                            this.pointIndex
                          ].percent * 100
                        ).toFixed(1);
                      return this.pointName + " (" + percentValue + "%)";
                    }
                  },
                  tooltip: {
                    enabled: $tooltipShow,
                    format: {
                      type: $tooltipType,
                      precision: $tooltipPrec,
                      currency: $tooltipCurr
                    }
                  },
                  export: {
                    enabled: $exportShow
                  },
                  resolveLabelOverlapping: $labelOverlap,
                  adaptiveLayout: {
                    width: $adaptLayout
                  },
                  animation: $animation,
                  series: $series
                });

                setupPrint(
                  $("#subvertSpendPrint"),
                  $("#subvertSpend"),
                  $("#subvertSpendPrintTooltip")
                );
                setupExport(
                  $("#subvertSpendExport"),
                  $("#subvertSpend"),
                  $("#subvertSpendExportTooltip")
                );
              }
            },
            error: function(err) {
              console.log("Error: Cannot Reach API");
            }
          });

          //Get Online Spending by Ad Platform
          var platformSpend = [],
            platformSpendUrl =
              "https://advantage.bia.com/api/MediaAdView/GetOnlineAdSpendingShareBySegment/" +
              $market +
              "/" +
              $subVertical +
              "/" +
              ($initYear - 1) +
              "/" +
              $initYear;
          $.ajax({
            url: platformSpendUrl,
            async: true,
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            headers: {
              "x-api-version": "2.0"
            },
            beforeSend: function(xhr) {
              xhr.setRequestHeader("Authorization", "Bearer " + $token);
            },
            success: function(platformSpendData) {
              if (platformSpendData) {
                $.each(platformSpendData, function(index, $v) {
                  if ($v.year == $initYear) {
                    var platformSpendTotal = $v.total,
                      $platformSpendTotal = $(".platformSpend-total");
                    if (platformSpendTotal > 0) {
                      $platformSpendTotal.text(
                        "$" +
                          platformSpendTotal
                            .toFixed(0)
                            .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +
                          " (000)s"
                      );
                    } else {
                      $platformSpendTotal.text("$0");
                    }
                    platformSpend = [
                      {
                        category: "Classified & Vertical",
                        marketTotal: $v.classifieds
                      },
                      {
                        category: "Local Other Display",
                        marketTotal: $v.otherDisplay
                      },
                      {
                        category: "Local Video",
                        marketTotal: $v.videoDisplay
                      },
                      {
                        category: "Local Search",
                        marketTotal: $v.search
                      }
                    ];
                  }
                });

                $("#platformSpend").dxPieChart({
                  type: "doughnut",
                  palette: $pallet,
                  dataSource: platformSpend,
                  legend: {
                    visible: $legendShow,
                    rowCount: 3,
                    verticalAlignment: "bottom",
                    horizontalAlignment: "center",
                    itemTextPosition: "right",
                    customizeText: function() {
                      var pieChart = $("#platformSpend").dxPieChart("instance"),
                        percentValue = (
                          pieChart.getAllSeries()[0].getAllPoints()[
                            this.pointIndex
                          ].percent * 100
                        ).toFixed(1);
                      return this.pointName + " (" + percentValue + "%)";
                    }
                  },
                  tooltip: {
                    enabled: $tooltipShow,
                    format: {
                      type: $tooltipType,
                      precision: $tooltipPrec,
                      currency: $tooltipCurr
                    }
                  },
                  export: {
                    enabled: $exportShow
                  },
                  resolveLabelOverlapping: $labelOverlap,
                  adaptiveLayout: {
                    width: $adaptLayout
                  },
                  animation: $animation,
                  series: $series
                });

                setupPrint(
                  $("#platformSpendPrint"),
                  $("#platformSpend"),
                  $("#platformSpendPrintTooltip")
                );
                setupExport(
                  $("#platformSpendExport"),
                  $("#platformSpend"),
                  $("#platformSpendExportTooltip")
                );
              }
            },
            error: function(err) {
              console.log("Error: Cannot Reach API");
            }
          });

          //Get Media Ad Spending
          var subvertUrl =
              "https://advantage.bia.com/api/MediaAdView/GetAnnualAdRevenueGrowth/" +
              $market +
              "/" +
              $subVertical +
              "/" +
              ($initYear - 1) +
              "/" +
              $initYear,
            subvertSpendTbl = [],
            csubvertSpend = [];
          $.ajax({
            url: subvertUrl,
            async: true,
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            headers: {
              "x-api-version": "2.0"
            },
            beforeSend: function(xhr) {
              xhr.setRequestHeader("Authorization", "Bearer " + $token);
            },
            success: function(subvertdata) {
              if (subvertdata) {
                var psubvertRevenues = subvertdata.verticalEstimatedRevenues[0],
                  csubvertSpendRevenues =
                    subvertdata.verticalEstimatedRevenues[1];
                $.each(psubvertRevenues.mediaTotals, function(index, value) {
                  subvertSpendTbl.push({
                    category: value.name,
                    pTotal: value.value
                  });
                });
                $.each(csubvertSpendRevenues.mediaTotals, function(
                  index,
                  value
                ) {
                  csubvertSpend.push({
                    cTotal: value.value,
                    percent: value.percentage,
                    change: value.percentChange
                  });
                });
                $.extend(true, subvertSpendTbl, csubvertSpend);

                hideLoadPanel($("#loadPanel"));
              }

              $("#subvertTable").dxDataGrid({
                dataSource: subvertSpendTbl,
                export: {
                  enabled: true,
                  fileName: $initYear + " Vertical Ad Spending",
                  allowExportSelectedData: false
                },
                onCellPrepared: function(options) {
                  if (options.value) {
                    if (options.column.dataField == "change") {
                      if (options.value < 0) {
                        options.cellElement.addClass("negative");
                      } else {
                        options.cellElement.addClass("positive");
                      }
                    }
                  }
                },
                summary: {
                  totalItems: [
                    {
                      column: "pTotal",
                      summaryType: "sum",
                      customizeText: function(data) {
                        return (
                          "$" +
                          data.value
                            .toFixed(0)
                            .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
                        );
                      }
                    },
                    {
                      column: "cTotal",
                      summaryType: "sum",
                      customizeText: function(data) {
                        return (
                          "$" +
                          data.value
                            .toFixed(0)
                            .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
                        );
                      }
                    }
                  ]
                },
                columns: [
                  {
                    dataField: "category",
                    caption: "Media Type",
                    sortOrder: "asc"
                  },
                  {
                    dataField: "pTotal",
                    caption: $initYear - 1 + " Local Advertising",
                    format: "currency"
                  },
                  {
                    dataField: "cTotal",
                    caption: $initYear + " Local Advertising",
                    format: "currency"
                  },
                  {
                    dataField: "percent",
                    caption: "Percent of Total",
                    format: {
                      type: "percent",
                      precision: 1
                    }
                  },
                  {
                    dataField: "change",
                    caption: "Annual Change",
                    format: {
                      type: "percent",
                      precision: 1
                    }
                  }
                ]
              });
            },
            error: function(err) {
              console.log("Error: Cannot Reach API");
            }
          });
        }
      });
    }
  }
});
