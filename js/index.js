$(function() {
  "use strict";
  //Chart Config
  var $initYear = "2018",
    $endYear = "2022",
    $placeholder = $(".content img"),
    $widget = $(".dxchart.widget"),
    $token = authToken(),
    $pallet = "Soft Pastel",
    $legendShow = false,
    $tooltipShow = true,
    $exportShow = false,
    $labelOverlap = "shift",
    $adaptLayout = 0,
    $tooltipType = "currency",
    $tooltipPrec = 0,
    $tooltipCurr = "USD",
    $animation = { enabled: "easeOutCubic", duration: 500 },
    $series = [
      {
        argumentField: "type",
        valueField: "total",
        border: {
          visible: "true",
          width: 0.5
        },
        label: {
          visible: true,
          displayMode: "stagger",
          staggeringSpacing: 20,
          position: "inside",
          font: {
            size: 16
          },
          connector: {
            visible: true,
            width: 1
          },
          border: {
            visible: false,
            color: "rgba(0, 0, 0, 0.25)",
            width: 1
          },
          format: "fixedPoint",
          percentPrecision: 1,
          customizeText: function(point) {
            return point.argumentText + " (" + point.percentText + ")";
          }
        },
        minSegmentSize: 5
      }
    ];

  initLoad();

  $(document).on("navSelected", "a.index.selected", function() {
    $(".select-item.market.main-market, .navbar-brand").removeClass("enable");
    $(".nav a.default").addClass("disabled");
    $(".select-item.subvert").removeClass("enable");

    initLoad();
  });

  function initLoad() {
    $placeholder.removeClass("disabled");
    $widget.addClass("disabled");

    setup();

    if (authToken()) {
      $placeholder.addClass("disabled");
      $widget.removeClass("disabled");

      $(".userName").text(getUserName());

      // get the available years async from the API
      var years = getAvailableYears().fail(function(
        jqXHR,
        textStatus,
        errorThrown
      ) {
        switch (jqXHR.status) {
          case 400:
          case 401:
            location.reload();
            break;
          case 403:
            alert("Error 403: Access denied");
            location.reload();
            break;
        }
      });

      showLoadPanel($("#loadPanel"), $(".cd-main"), true);

      setupNavOptions();

      var totalMktsLoaded = $(".main-market .filters-market option").length - 1;
      if (totalMktsLoaded <= 0) {
        //Market Select
        getJson("https://advantage.bia.com/api/Markets")
          .done(function(mktdata) {
            var ddlMarkets = $(".main-market .filters-market");
            ddlMarkets.empty();

            $.each(mktdata, function(index, value) {
              var option = $("<option />");
              option
                .addClass("button")
                .html(value.marketName)
                .val(value.marketNbr)
                .attr("data-rank", value.marketRank)
                .attr(
                  "data-radio-market",
                  value.marketRadioNbr === 0 ? "false" : "true"
                )
                .attr("data-obj", btoa(JSON.stringify(value))); // I don't like this but for now it must work
              ddlMarkets.append(option);

              // it would be really nice to store the json object that is returned
              // using juery.data to eliminate round trips to the
              // to the api and based on the selection[key] you would
              // know if they have radio markets and you would have the
              // city/state as well. That way seems much easier to me.
              //$(".main-market .filters-market").data(value.marketNbr, value);
            });

            var $mkts = $(".main-market .filters-market option").clone();

            $(".init-market .filters-market")
              .append($mkts)
              .dropdown();

            $(".main-market .filters-market").dropdown();
          })
          .fail(function(jqXHR, textStatus, errorThrown) {
            // if we can't get the markets then we should probably
            // check the return code to see if don't have markets
            // or if we are receiving an auth error and need to prompt
            // for login.

            alert(textStatus);
          });
      } else {
        // we already have the markets, no need to query the api again
        var $mkts = $(".main-market .filters-market option").clone();

        $(".init-market .filters-market")
          .append($mkts)
          .dropdown({
            metadata: {
              defaultText: "Type Market Name...",
              placeholderText: "Type Market Name..."
            }
          });
      }

      $(".select-item.home-market").on("change.index", "select", function() {
        var currentMarket = $(this)
          .children("option:selected")
          .val();
        var data = $(this)
          .children("option:selected")
          .attr("data-obj");
        $("#nav").trigger("marketChanged", [
          { selectedMarket: currentMarket, mktObj: data }
        ]);
      });

      $(".select-item.quick-subvert").on("change.index", "select", function() {
        var currentMarket = $(".quick-market .filters-market").dropdown(
          "get value"
        );
        var currentVertical = $(this)
          .children("option:selected")
          .val();
        var data = $(this)
          .children("option:selected")
          .attr("data-obj");
        $("#nav").trigger("qmarketChanged", [
          {
            selectedMarket: currentMarket,
            vertical: currentVertical,
            mktObj: data
          }
        ]);
      });

      //Subvertical Select
      var subvUrl = "https://advantage.bia.com/api/SubVerticals";
      getJson(subvUrl)
        .done(function(subvdata) {
          $.each(subvdata, function(index, value) {
            $(".quick-subvert .filters-subvert").append(
              '<option class="button" data-menu="subvertical" value= "' +
                value.id +
                '">' +
                value.name +
                "</option>"
            );
            $(".filters-subvert .menu").append(
              '<div class="item" data-menu="subvertical" data-value="' +
                value.id +
                '">' +
                value.name +
                "</div>"
            );
          });

          $(".quick-subvert ").dropdown();
          $(".filters-subvert").dropdown({
            onChange: function(val, text, choice) {
              if (val) {
                var currentMarket = getSelectedMarket().val();
                var vertId = $(this).data("vertical-id");
                var subVertId = $(this).data("value");

                $("#nav").trigger("subverticalChanged", [
                  {
                    selectedMarket: currentMarket,
                    vertical: vertId,
                    subvertical: subVertId
                  }
                ]);
              }
            }
          });
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          alert(textStatus);
        });

      //Get Local Forcast by Media Type
      var natForcastFirstTotals = [],
        natForcastLastTotals = [],
        natForcastTable = [],
        natForcastFirstUrl =
          "https://advantage.bia.com/api/MediaAdView/GetNationwideMarketSummary/" +
          $initYear,
        natForcastLastUrl =
          "https://advantage.bia.com/api/MediaAdView/GetNationwideMarketSummary/" +
          $endYear;
      getJson(natForcastFirstUrl)
        .done(function(natForcastFirst) {
          if (natForcastFirst) {
            //Populate natForcast
            natForcastFirstTotals = [
              {
                type: "Online",
                total: natForcastFirst.onlineTotal
              },
              {
                type: "Print",
                total: natForcastFirst.printTotal
              },
              {
                type: "Other",
                total: natForcastFirst.otherTotal
              }
            ];
          }

          $.ajax({
            url: natForcastLastUrl,
            async: true,
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            headers: { "x-api-version": "2.0" },
            beforeSend: function(xhr) {
              xhr.setRequestHeader("Authorization", "Bearer " + $token);
            },
            success: function(natForcastLast) {
              if (natForcastLast) {
                //Populate natForcast
                natForcastLastTotals = [
                  {
                    type: "Online",
                    total: natForcastLast.onlineTotal
                  },
                  {
                    type: "Print",
                    total: natForcastLast.printTotal
                  },
                  {
                    type: "Other",
                    total: natForcastLast.otherTotal
                  }
                ];

                $.each(natForcastFirst.mediaTotals, function(index, value) {
                  natForcastTable.push({
                    name: value.name,
                    yFirst: value.marketTotal,
                    yLast: natForcastLast.mediaTotals.find(item => {
                      return item.name == value.name;
                    }).marketTotal,
                    type: value.mediaType
                  });
                });
              }

              //Create natForcast Charts
              $("#natForcastFirst").dxPieChart({
                palette: $pallet,
                dataSource: natForcastFirstTotals,
                legend: {
                  visible: $legendShow
                },
                tooltip: {
                  enabled: $tooltipShow,
                  format: {
                    type: $tooltipType,
                    precision: $tooltipPrec,
                    currency: $tooltipCurr
                  }
                },
                export: {
                  enabled: $exportShow
                },
                resolveLabelOverlapping: $labelOverlap,
                adaptiveLayout: {
                  width: $adaptLayout
                },
                animation: $animation,
                series: $series
              });

              $("#natForcastLast").dxPieChart({
                palette: $pallet,
                dataSource: natForcastLastTotals,
                legend: {
                  visible: $legendShow
                },
                tooltip: {
                  enabled: $tooltipShow,
                  format: {
                    type: $tooltipType,
                    precision: $tooltipPrec,
                    currency: $tooltipCurr
                  }
                },
                export: {
                  enabled: $exportShow
                },
                resolveLabelOverlapping: $labelOverlap,
                adaptiveLayout: {
                  width: $adaptLayout
                },
                animation: $animation,
                series: $series
              });

              //Create natForcastTable
              $("#natForcastTable").dxDataGrid({
                dataSource: natForcastTable,
                export: {
                  enabled: true,
                  fileName: "Local Forcast by Media Type",
                  allowExportSelectedData: false
                },
                selection: {
                  mode: "single"
                },
                columns: [
                  {
                    dataField: "name",
                    caption: "Media",
                    sortOrder: "asc"
                  },
                  {
                    dataField: "yFirst",
                    caption: $initYear,
                    format: "currency"
                  },
                  {
                    dataField: "yLast",
                    caption: $endYear,
                    format: "currency"
                  },
                  {
                    dataField: "type",
                    caption: "Media Type",
                    groupIndex: 0
                  }
                ],
                sortByGroupSummaryInfo: [
                  {
                    summaryItem: "count"
                  }
                ],
                summary: {
                  totalItems: [
                    {
                      column: "name",
                      summaryType: "count",
                      alignment: "right",
                      displayFormat: "Nationwide Total:",
                      showInColumn: "name"
                    },
                    {
                      column: $initYear,
                      summaryType: "sum",
                      alignment: "right",
                      valueFormat: "currency",
                      displayFormat: "{0}",
                      showInColumn: $initYear
                    },
                    {
                      column: $endYear,
                      summaryType: "sum",
                      alignment: "right",
                      valueFormat: "currency",
                      displayFormat: "{0}",
                      showInColumn: $endYear
                    }
                  ],
                  groupItems: [
                    {
                      column: "name",
                      summaryType: "count",
                      displayFormat: "{0}"
                    },
                    {
                      column: $initYear,
                      summaryType: "sum",
                      valueFormat: "currency",
                      displayFormat: "{0}",
                      showInGroupFooter: true
                    },
                    {
                      column: $endYear,
                      summaryType: "sum",
                      valueFormat: "currency",
                      displayFormat: "{0}",
                      showInGroupFooter: true
                    }
                  ]
                }
              });

              hideLoadPanel($("#loadPanel"));
            },
            error: function(err) {
              console.log("Error: Cannot Reach API");
            }
          });
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          switch (jqXHR.status) {
            case 400:
            case 401:
              location.reload();
              break;

            case 403:
              alert("Error 403: Access denied");
              location.reload();
              break;
          }
        });
    }
  }
});
