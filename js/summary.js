$(document).on("navSelected", "a.summary.selected", function() {
  //Chart Config
  var $market = 1,
    $mktRank = $(".mkt-rank"),
    $mktTitle = $(".mkt-title"),
    $initYear = "2018",
    $endYear = "2022",
    $map,
    $layer,
    $geoLat,
    $geoLng,
    $vertTotal = $(".vert-total"),
    $pallet = "Default",
    $palletLight = "Bright",
    $legendShow = true,
    $tooltipShow = true,
    $exportShow = false,
    $labelOverlap = "shift",
    $adaptLayout = 10,
    $tooltipType = "currency",
    $tooltipPrec = 0,
    $tooltipCurr = "USD",
    $animation = {enabled: false},
    $legend = {},
    $series = [
      {
        argumentField: "category",
        valueField: "marketTotal",
        border: {
          visible: "true",
          width: 0.5
        },
        label: {
          visible: true,
          displayMode: "stagger",
          staggeringSpacing: 15,
          position: "columns",
          font: {
            size: 12
          },
          connector: {
            visible: true,
            width: 1
          },
          border: {
            visible: true,
            color: "rgba(0, 0, 0, 0.25)",
            width: 1
          },
          format: "fixedPoint",
          percentPrecision: 1,
          customizeText: function(pnt) {
            var series = pnt.point.series.getAllPoints();
            series.sort(
              (a, b) => Number(b.percent * 100) - Number(a.percent * 100)
            );

            var pntIndex = series.findIndex(x => x.argument == pnt.argument);

            // only display the top 8
            // counting on the sort order to max -> min
            if (pntIndex < 8) {
              return "<b>" + pnt.percentText + "</b><br>" + pnt.argumentText;
            }

            return "";
          }
        },
        minSegmentSize: 4
      }
    ];

  loadSummaryContent();

   function loadSummaryContent() {
    $(".select-item.market").on("change.summary", function(event) {
      if (event.originalEvent && $(".summary").hasClass("selected")) {
        loadSummaryContent();
      } else {
        $(".select-item.market").off("change.summary");
      }
    });

    $market = getSelectedMarket().val();

    if ($market != "") {
      var $token = authToken(),
        $mktSelected = getSelectedMarket().text();

      $mktGeo = $mktSelected.split(" ").join("+");
      $mktRank.text(getSelectedMarket().data("rank"));

      //refresh session
      $token ? setAuthToken($token) : location.reload();
      mktTitle($mktSelected);
      function mktTitle() {
        if ($(".summary .mkt-title").length < 2) {
          window.requestAnimationFrame(mktTitle);
        } else {
          $(".summary .mkt-title, .navbar-brand .mkt-title").text($mktSelected);
        }
      }

      $(".year a").text($initYear);
      $(".button.prev").addClass("disabled");

      showLoadPanel($("#loadPanel"), $(".cd-main"), true);

      $(".select-item.subvert").removeClass("enable");
      $(".select-item.market.main-market, .navbar-brand").addClass("enable");

      $(".nav a").not(
          $(".nav a.radio"),
          $(".nav a.mapro"),
          $(".nav a.survey"),
          $(".nav a.reports"),
          $(".nav a.ad-forecast"))
          .removeClass("disabled");

      var marketObj = JSON.parse(atob(getSelectedMarket().attr("data-obj")));
      // get the market json object returned from the API
      if (!$.isEmptyObject(marketObj) && marketObj.marketRadioNbr == 0) {
          // if they don't have a radio market then
          // let's disable the radio OTA menu option
          $(".nav a.radio").addClass("disabled");
      } else {
          $(".nav a.radio").removeClass("disabled");
      }

      loadMap($map, $mktGeo);
      function loadMap($map, $mktGeo) {
        if ($(".map-container").prevObject.length < 1 && !$mktGeo) {
          window.requestAnimationFrame(loadMap);
        } else {
          //Update Market Map
          var geoUrl =
            "https://maps.googleapis.com/maps/api/geocode/json?address=" +
            $mktGeo +
            "&key=AIzaSyCmqKtpuTeurbHzrrMi6kg67r1fD4sXXgQ";
          $.ajax({
            url: geoUrl,
            async: true,
            type: "GET",
            dataType: "json",
            success: function(geodata) {
              if (geodata.results.length > 0) {
                var geoCoord = geodata.results[0].geometry.location;
                $geoLat = geoCoord.lat;
                $geoLng = geoCoord.lng;
                $("#map").remove();
                $(".map-container").append(
                  '<div id="map" class="coverage-map"></div>'
                );
                initMapCheck($geoLat, $geoLng);
                function initMapCheck($map, $mktGeo) {
                  if ($("#map").length < 1) {
                    window.requestAnimationFrame(initMapCheck);
                  } else {
                    mapInit($geoLat, $geoLng);
                  }
                }
                function mapInit() {
                  // Initialize MAP
                  $map = L.map("map", {
                    center: [$geoLat, $geoLng],
                    zoom: 7,
                    scrollWheelZoom: false,
                    attributionControl: false,
                    layers: [
                      L.tileLayer(
                        "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                        {
                          attribution: ""
                        }
                      )
                    ]
                  });
                }

                if (typeof $layer != "undefined") {
                  $layer.clearLayers();
                  $map.panTo(new L.LatLng($geoLat, $geoLng));
                }
                $.getJSON(
                  "https://s3-us-west-2.amazonaws.com/s.cdpn.io/532833/" +
                    $market +
                    ".geo.json",
                  function(geojson) {
                    $layer = L.geoJson(geojson, {
                      onEachFeature: function(feature, layer) {
                        layer.bindPopup(feature.geographicRegion);
                      }
                    }).addTo($map);
                  }
                );
              }
            },
            error: function(err) {
              console.log("Error: Cannot Reach Google Maps");
            }
          });
        }
      }

      //Get Local Ad Spend
      var locSpend = [],
        locSpendCompare = [],
        locSpendUrl =
          getBaseHref() +
          "api/MediaAdView/GetMarketSummary/" +
          $market +
          "/" +
          $initYear;
      $.ajax({
        url: locSpendUrl,
        async: true,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        headers: { "x-api-version": "2.0" },
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Bearer " + $token);
        },
        success: function(locSpendData) {
          if (locSpendData) {
            var locSpendTotal = locSpendData.marketTotal,
              $locSpendTotal = $(".locSpend-total");
            $locSpendTotal.text(
              locSpendTotal < 999999999
                ? "$" + (locSpendTotal / 1000000).toFixed(0) + " Million"
                : "$" + (locSpendTotal / 1000000000).toFixed(1) + " Billion"
            );
            $.each(locSpendData.mediaTotals, function(index, value) {
              locSpend.push({
                category: value.name,
                marketTotal: value.marketTotal
              });
            });

            $("#locSpend").dxPieChart({
              palette: $pallet,
              dataSource: locSpend,
              legend: {
                visible: $legendShow,
                rowCount: 3,
                verticalAlignment: "bottom",
                horizontalAlignment: "center",
                itemTextPosition: "right",
                customizeText: function() {
                  var pieChart = $("#locSpend").dxPieChart("instance"),
                    percentValue = (
                      pieChart.getAllSeries()[0].getAllPoints()[this.pointIndex]
                        .percent * 100
                    ).toFixed(1);
                  return this.pointName + " (" + percentValue + "%)";
                }
              },
              tooltip: {
                enabled: $tooltipShow,
                format: {
                  type: "fixedPoint",
                  precision: 1
                },
                customizeTooltip: function(args) {
                  return {
                    html:
                      "<div><h5>" +
                      args.argumentText +
                      "</h5></div>" +
                      "<div><b>Amount: </b>" +
                      args.valueText +
                      "</div>" +
                      "<div><b>Percentage: </b>" +
                      (args.percent * 100).toFixed(1) +
                      " %</div>"
                  };
                }
              },
              export: {
                enabled: $exportShow
              },
              resolveLabelOverlapping: $labelOverlap,
              adaptiveLayout: {
                width: $adaptLayout
              },
              animation: $animation,
              series: $series,
              onLegendClick: function(info) {
                var pieChart = info.component;
                var argument = info.target;
                pieChart
                  .getAllSeries()[0]
                  .getPointsByArg(argument)[0]
                  .showTooltip();
              }
            });

            setupPrint($("#locSpendPrint"), $("#locSpend"), $("#printTooltip"));
            setupExport($("#locSpendExport"), $("#locSpend"), $("#exportTooltip"));
            setupAdSpendTrigger(
              $("#adSpendCompareTrigger"),
              $("#adSpendCompareTooltip")
            );

            $("#locSpendCompare").dxPieChart({
              palette: $pallet,
              dataSource: locSpend,
              legend: {
                visible: $legendShow,
                rowCount: 3,
                verticalAlignment: "bottom",
                horizontalAlignment: "center",
                itemTextPosition: "right",
                customizeText: function() {
                  var pieChart = $("#locSpendCompare").dxPieChart("instance"),
                    percentValue = (
                      pieChart.getAllSeries()[0].getAllPoints()[this.pointIndex]
                        .percent * 100
                    ).toFixed(1);
                  return this.pointName + " (" + percentValue + "%)";
                }
              },
              tooltip: {
                enabled: $tooltipShow,
                format: {
                  type: $tooltipType,
                  precision: $tooltipPrec,
                  currency: $tooltipCurr
                },
                customizeTooltip: function(args) {
                  return {
                    html:
                      "<div><h5>" +
                      args.argumentText +
                      "</h5></div>" +
                      "<div><b>Amount: </b>" +
                      args.valueText +
                      "</div>" +
                      "<div><b>Percentage: </b>" +
                      (args.percent * 100).toFixed(1) +
                      " %</div>"
                  };
                }
              },
              export: {
                enabled: $exportShow
              },
              resolveLabelOverlapping: $labelOverlap,
              adaptiveLayout: {
                width: $adaptLayout
              },
              animation: $animation,
              series: $series,
              onLegendClick: function(info) {
                var pieChart = info.component;
                var argument = info.target;
                pieChart
                  .getAllSeries()[0]
                  .getPointsByArg(argument)[0]
                  .showTooltip();
              }
            });
            
            setupComparePrint($("#natLocSpendPrint"), $("#natSpend"), $("#locSpendCompare"), $("#printTooltip"));

            setupCompareExport(
              $("#natLocSpendExport"),
              $("#locSpendCompare"),
              $("#natSpend"),
              $("#exportNatLocTooltip")
            );
            setupAdSpendTrigger(
              $("#adSpendCompareShrinkTrigger"),
              $("#adSpendCompareShrinkTooltip"),
              "Close",
              "fa fa-compress"
            );
          }
        },
        error: function(err) {
          console.log("Error: Cannot Reach API");
        }
      });

      //Update Year Selection
      locSpendPrev();
      function locSpendPrev() {
        if ($(".locSpend .button.prev").length < 1) {
          window.requestAnimationFrame(locSpendPrev);
        } else {
          $(".locSpend .button.prev").unbind();
          $(".locSpend .button.prev").on("click", function() {
            $(".locSpend .button.next").removeClass("disabled");
            var $year = $(".locSpend .year").text(),
              $yearNum = parseInt($year);
            if ($yearNum != $initYear) {
              $(".locSpend .year a").text($yearNum - 1);
            }
            if ($yearNum == $initYear + 1) {
              $(".locSpend .button.prev").addClass("disabled");
            }
            var $locSpendYr = $(".locSpend .year").text(),
              locSpendUrl =
                getBaseHref() +
                "api/MediaAdView/GetMarketSummary/" +
                $market +
                "/" +
                $locSpendYr;
            $.ajax({
              url: locSpendUrl,
              async: true,
              type: "GET",
              dataType: "json",
              contentType: "application/json",
              headers: { "x-api-version": "2.0" },
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $token);
              },
              success: function(locSpendData) {
                if (locSpendData) {
                  var locSpendTotal = locSpendData.marketTotal,
                    $locSpendTotal = $(".locSpend-total");
                  $locSpendTotal.text(
                    locSpendTotal < 999999999
                      ? "$" + (locSpendTotal / 1000000).toFixed(0) + " Million"
                      : "$" +
                        (locSpendTotal / 1000000000).toFixed(1) +
                        " Billion"
                  );
                  locSpend = [];
                  $.each(locSpendData.mediaTotals, function(index, value) {
                    locSpend.push({
                      category: value.name,
                      marketTotal: value.marketTotal
                    });
                  });
                  $("#locSpend").dxPieChart("option", "dataSource", locSpend);
                }
              },
              error: function(err) {
                console.log("Error: Cannot Reach API");
              }
            });
          });
        }
      }

      locSpendNext();
      function locSpendNext() {
        if ($(".locSpend .button.next").length < 1) {
          window.requestAnimationFrame(locSpendNext);
        } else {
          $(".locSpend .button.next").unbind();
          $(".locSpend .button.next").on("click", function() {
            $(".locSpend .button.prev").removeClass("disabled");
            var $year = $(".locSpend .year").text(),
              $yearNum = parseInt($year);
            if ($yearNum != $endYear) {
              $(".locSpend .year a").text($yearNum + 1);
            }
            if ($yearNum == $endYear - 1) {
              $(".locSpend .button.next").addClass("disabled");
            }
            var $locSpendYr = $(".locSpend .year").text(),
              locSpendUrl =
                getBaseHref() +
                "api/MediaAdView/GetMarketSummary/" +
                $market +
                "/" +
                $locSpendYr;
            $.ajax({
              url: locSpendUrl,
              async: true,
              type: "GET",
              dataType: "json",
              contentType: "application/json",
              headers: { "x-api-version": "2.0" },
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $token);
              },
              success: function(locSpendData) {
                if (locSpendData) {
                  var locSpendTotal = locSpendData.marketTotal,
                    $locSpendTotal = $(".locSpend-total");
                  $locSpendTotal.text(
                    locSpendTotal < 999999999
                      ? "$" + (locSpendTotal / 1000000).toFixed(0) + " Million"
                      : "$" +
                        (locSpendTotal / 1000000000).toFixed(1) +
                        " Billion"
                  );
                  locSpend = [];
                  $.each(locSpendData.mediaTotals, function(index, value) {
                    locSpend.push({
                      category: value.name,
                      marketTotal: value.marketTotal
                    });
                  });
                  $("#locSpend").dxPieChart("option", "dataSource", locSpend);
                }
              },
              error: function(err) {
                console.log("Error: Cannot Reach API");
              }
            });
          });
        }
      }

      //Update Compare Year Selection
      locSpendComparePrev();
      function locSpendComparePrev() {
        if ($(".locSpendCompare .button.prev").length < 1) {
          window.requestAnimationFrame(locSpendComparePrev);
        } else {
          $(".locSpendCompare .button.prev").unbind();
          $(".locSpendCompare .button.prev").on("click", function() {
            $(".locSpendCompare .button.next").removeClass("disabled");
            var $year = $(".locSpendCompare .year").text(),
              $yearNum = parseInt($year);
            if ($yearNum != $initYear) {
              $(".locSpendCompare .year a").text($yearNum - 1);
            }
            if ($yearNum == $initYear + 1) {
              $(".locSpendCompare .button.prev").addClass("disabled");
            }
            var $locSpendYr = $(".locSpendCompare .year").text(),
              locSpendUrl =
                getBaseHref() +
                "api/MediaAdView/GetMarketSummary/" +
                $market +
                "/" +
                $locSpendYr;
            $.ajax({
              url: locSpendUrl,
              async: true,
              type: "GET",
              dataType: "json",
              contentType: "application/json",
              headers: { "x-api-version": "2.0" },
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $token);
              },
              success: function(locSpendData) {
                if (locSpendData) {
                  var locSpendTotal = locSpendData.marketTotal,
                    $locSpendTotal = $(".locSpend-total");
                  $locSpendTotal.text(
                    locSpendTotal < 999999999
                      ? "$" + (locSpendTotal / 1000000).toFixed(0) + " Million"
                      : "$" +
                        (locSpendTotal / 1000000000).toFixed(1) +
                        " Billion"
                  );
                  locSpendCompare = [];
                  $.each(locSpendData.mediaTotals, function(index, value) {
                    locSpendCompare.push({
                      category: value.name,
                      marketTotal: value.marketTotal
                    });
                  });
                  $("#locSpendCompare").dxPieChart(
                    "option",
                    "dataSource",
                    locSpendCompare
                  );
                }
              },
              error: function(err) {
                console.log("Error: Cannot Reach API");
              }
            });
          });
        }
      }

      locSpendCompareNext();
      function locSpendCompareNext() {
        if ($(".locSpendCompare .button.next").length < 1) {
          window.requestAnimationFrame(locSpendCompareNext);
        } else {
          $(".locSpendCompare .button.next").unbind();
          $(".locSpendCompare .button.next").on("click", function() {
            $(".locSpendCompare .button.prev").removeClass("disabled");
            var $year = $(".locSpendCompare .year").text(),
              $yearNum = parseInt($year);
            if ($yearNum != $endYear) {
              $(".locSpendCompare .year a").text($yearNum + 1);
            }
            if ($yearNum == $endYear - 1) {
              $(".locSpendCopare .button.next").addClass("disabled");
            }
            var $locSpendYr = $(".locSpendCompare .year").text(),
              locSpendCompUrl =
                getBaseHref() +
                "api/MediaAdView/GetMarketSummary/" +
                $market +
                "/" +
                $locSpendYr;
            $.ajax({
              url: locSpendCompUrl,
              async: true,
              type: "GET",
              dataType: "json",
              contentType: "application/json",
              headers: { "x-api-version": "2.0" },
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $token);
              },
              success: function(locSpendData) {
                if (locSpendData) {
                  var locSpendTotal = locSpendData.marketTotal,
                    $locSpendTotal = $(".locSpend-total");
                  $locSpendTotal.text(
                    locSpendTotal < 999999999
                      ? "$" + (locSpendTotal / 1000000).toFixed(0) + " Million"
                      : "$" +
                        (locSpendTotal / 1000000000).toFixed(1) +
                        " Billion"
                  );
                  locSpendCompare = [];
                  $.each(locSpendData.mediaTotals, function(index, value) {
                    locSpendCompare.push({
                      category: value.name,
                      marketTotal: value.marketTotal
                    });
                  });
                  $("#locSpendCompare").dxPieChart(
                    "option",
                    "dataSource",
                    locSpendCompare
                  );
                }
              },
              error: function(err) {
                console.log("Error: Cannot Reach API");
              }
            });
          });
        }
      }

      // Get Natl Ad Spend
      var natSpend = [],
        natSpendUrl =
          getBaseHref() +
          "api/MediaAdView/GetNationwideMarketSummary/" +
          "/" +
          $initYear;
      $.ajax({
        url: natSpendUrl,
        async: true,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        headers: { "x-api-version": "2.0" },
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Bearer " + $token);
        },
        success: function(natSpendData) {
          if (natSpendData) {
            var natSpendTotal = natSpendData.marketTotal,
              $natSpendTotal = $(".natSpend-total");
            $natSpendTotal.text(
              natSpendTotal < 999999999
                ? "$" + (natSpendTotal / 1000000).toFixed(0) + " Million"
                : "$" + (natSpendTotal / 1000000000).toFixed(1) + " Billion"
            );
            $.each(natSpendData.mediaTotals, function(index, value) {
              natSpend.push({
                category: value.name,
                marketTotal: value.marketTotal
              });
            });
            $("#natSpend").dxPieChart({
              palette: $pallet,
              dataSource: natSpend,
              legend: {
                visible: $legendShow,
                rowCount: 3,
                verticalAlignment: "bottom",
                horizontalAlignment: "center",
                itemTextPosition: "right",
                customizeText: function() {
                  var pieChart = $("#natSpend").dxPieChart("instance"),
                    percentValue = (
                      pieChart.getAllSeries()[0].getAllPoints()[this.pointIndex]
                        .percent * 100
                    ).toFixed(1);
                  return this.pointName + " (" + percentValue + "%)";
                }
              },
              tooltip: {
                enabled: $tooltipShow,
                format: {
                  type: $tooltipType,
                  precision: $tooltipPrec,
                  currency: $tooltipCurr
                },
                customizeTooltip: function(args) {
                  return {
                    html:
                      "<div><h5>" +
                      args.argumentText +
                      "</h5></div>" +
                      "<div><b>Amount: </b>" +
                      args.valueText +
                      "</div>" +
                      "<div><b>Percentage: </b>" +
                      (args.percent * 100).toFixed(1) +
                      " %</div>"
                  };
                }
              },
              export: {
                enabled: $exportShow
              },
              resolveLabelOverlapping: $labelOverlap,
              adaptiveLayout: {
                width: $adaptLayout
              },
              animation: $animation,
              series: $series,
              onLegendClick: function(info) {
                var pieChart = info.component;
                var argument = info.target;
                pieChart
                  .getAllSeries()[0]
                  .getPointsByArg(argument)[0]
                  .showTooltip();
              }
            });
          }
        },
        error: function(err) {
          console.log("Error: Cannot Reach API");
        }
      });

      //Update Year Selection
      natSpendPrev();
      function natSpendPrev() {
        if ($(".natSpend .button.prev").length < 1) {
          window.requestAnimationFrame(natSpendPrev);
        } else {
          $(".natSpend .button.prev").unbind();
          $(".natSpend .button.prev").on("click", function() {
            $(".natSpend .button.next").removeClass("disabled");
            var $year = $(".natSpend .year").text(),
              $yearNum = parseInt($year);
            if ($yearNum != $initYear) {
              $(".natSpend .year a").text($yearNum - 1);
            }
            if ($yearNum == $initYear + 1) {
              $(".natSpend .button.prev").addClass("disabled");
            }
            var $natSpendYr = $(".natSpend .year").text(),
              natSpendUrl =
                getBaseHref() +
                "api/MediaAdView/GetNationwideMarketSummary/" +
                "/" +
                $natSpendYr;
            $.ajax({
              url: natSpendUrl,
              async: true,
              type: "GET",
              dataType: "json",
              contentType: "application/json",
              headers: { "x-api-version": "2.0" },
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $token);
              },
              success: function(natSpendData) {
                if (natSpendData) {
                  var natSpendTotal = natSpendData.marketTotal,
                    $natSpendTotal = $(".natSpend-total");
                  $natSpendTotal.text(
                    natSpendTotal < 999999999
                      ? "$" + (natSpendTotal / 1000000).toFixed(0) + " Million"
                      : "$" +
                        (natSpendTotal / 1000000000).toFixed(1) +
                        " Billion"
                  );
                  natSpend = [];
                  $.each(natSpendData.mediaTotals, function(index, value) {
                    natSpend.push({
                      category: value.name,
                      marketTotal: value.marketTotal
                    });
                  });
                  $("#natSpend").dxPieChart("option", "dataSource", natSpend);
                }
              },
              error: function(err) {
                console.log("Error: Cannot Reach API");
              }
            });
          });
        }
      }

      natSpendNext();
      function natSpendNext() {
        if ($(".natSpend .button.next").length < 1) {
          window.requestAnimationFrame(natSpendNext);
        } else {
          $(".natSpend .button.next").unbind();
          $(".natSpend .button.next").on("click", function() {
            $(".natSpend .button.prev").removeClass("disabled");
            var $year = $(".natSpend .year").text(),
              $yearNum = parseInt($year);
            if ($yearNum != $endYear) {
              $(".natSpend .year a").text($yearNum + 1);
            }
            if ($yearNum == $endYear - 1) {
              $(".natSpend .button.next").addClass("disabled");
            }
            var $natSpendYr = $(".natSpend .year").text(),
              natSpendUrl =
                getBaseHref() +
                "api/MediaAdView/GetNationwideMarketSummary/" +
                "/" +
                $natSpendYr;
            $.ajax({
              url: natSpendUrl,
              async: true,
              type: "GET",
              dataType: "json",
              contentType: "application/json",
              headers: { "x-api-version": "2.0" },
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $token);
              },
              success: function(natSpendData) {
                if (natSpendData) {
                  var natSpendTotal = natSpendData.marketTotal,
                    $natSpendTotal = $(".natSpend-total");
                  $natSpendTotal.text(
                    natSpendTotal < 999999999
                      ? "$" + (natSpendTotal / 1000000).toFixed(0) + " Million"
                      : "$" +
                        (natSpendTotal / 1000000000).toFixed(1) +
                        " Billion"
                  );
                  natSpend = [];
                  $.each(natSpendData.mediaTotals, function(index, value) {
                    natSpend.push({
                      category: value.name,
                      marketTotal: value.marketTotal
                    });
                  });
                  $("#natSpend").dxPieChart("option", "dataSource", natSpend);
                }
              },
              error: function(err) {
                console.log("Error: Cannot Reach API");
              }
            });
          });
        }
      }
      /*
        // Open Ad Spend Compare
        adSpendCompare();
        function adSpendCompare() {
          if ($(".adSpendCompareTrigger").length < 1) {
            window.requestAnimationFrame(adSpendCompare);
          } else {
            $(".adSpendCompareTrigger").unbind();
            $(".adSpendCompareTrigger").on("click", function() {
              console.log("compare");
              $(
                ".card.expand.adSpendCompare ,.adSpendCompare .header.expand ,.adSpendCompare .content.expand"
              ).toggleClass("expanded");
            });
          }
        }
*/
      //Get Local Forcast by Media Type
      var locForcast = [],
        locForcastTable = [],
        locForcastTableLast = [],
        locForcastUrl =
          getBaseHref() +
          "api/MediaAdView/GetTopVerticalsByMedia/" +
          $market +
          "/" +
          $initYear +
          "/" +
          $endYear;
      $.ajax({
        url: locForcastUrl,
        async: true,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        headers: { "x-api-version": "2.0" },
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Bearer " + $token);
        },
        success: function(locForcastdata) {
          if (locForcastdata) {
            //Populate locForcast
            var mediaTotalsFirst =
                locForcastdata[0].allMediaVerticalTotals.mediaTotals,
              mediaTotalsLast =
                locForcastdata[4].allMediaVerticalTotals.mediaTotals,
              otherTotal = 0,
              printTotal = 0,
              onlineTotal = 0,
              mType;
            $.each(mediaTotalsFirst, function(index, value) {
              if (
                value.name == "TV OTA" ||
                value.name == "Radio OTA" ||
                value.name == "Out of home" ||
                value.name == "Cable TV"
              ) {
                otherTotal = otherTotal + value.value;
              } else if (
                value.name == "Direct Mail" ||
                value.name == "Mags Print" ||
                value.name == "News Print" ||
                value.name == "Print YP"
              ) {
                printTotal = printTotal + value.value;
              } else {
                onlineTotal = onlineTotal + value.value;
              }
            });
            locForcast.push({
              year: $initYear,
              online: onlineTotal,
              print: printTotal,
              other: otherTotal
            });
            $.each(mediaTotalsLast, function(index, value) {
              if (
                value.name == "TV OTA" ||
                value.name == "Radio OTA" ||
                value.name == "Out of home" ||
                value.name == "Cable TV"
              ) {
                otherTotal += value.value;
              } else if (
                value.name == "Direct Mail" ||
                value.name == "Mags Print" ||
                value.name == "News Print" ||
                value.name == "Print YP"
              ) {
                printTotal += value.value;
              } else {
                onlineTotal += value.value;
              }
            });
            locForcast.push({
              year: $endYear,
              online: onlineTotal,
              print: printTotal,
              other: otherTotal
            });

            //Populate locForcastTable
            $.each(mediaTotalsFirst, function(index, value) {
              if (
                value.name == "TV OTA" ||
                value.name == "Radio OTA" ||
                value.name == "Out of home" ||
                value.name == "Cable TV"
              ) {
                mType = "Other Media";
              } else if (
                value.name == "Direct Mail" ||
                value.name == "Magazines Print" ||
                value.name == "Newspapers Print" ||
                value.name == "Print Yellow Pages"
              ) {
                mType = "Print Media";
              } else {
                mType = "Online Media";
              }
              locForcastTable.push({
                name: value.name,
                yFirst: value.value,
                type: mType
              });
            });
            $.each(mediaTotalsLast, function(index, value) {
              locForcastTableLast.push({
                yLast: value.adSpendingTotal,
                cagr: value.cagr
              });
            });
            $.extend(true, locForcastTable, locForcastTableLast);

            setTimeout(hideLoadPanel($("#loadPanel"), 1000));
          }

          //Create locForcast Chart
          $("#locForcast").dxChart({
            dataSource: locForcast,
            commonSeriesSettings: {
              argumentField: "year",
              type: "stackedBar"
            },
            series: [
              { valueField: "online", name: "Online Media" },
              { valueField: "print", name: "Print Media" },
              { valueField: "other", name: "Other Media" }
            ],
            legend: {
              verticalAlignment: "bottom",
              horizontalAlignment: "center",
              itemTextPosition: "top"
            },
            customizeLabel: function() {
              return {
                visible: true,
                customizeText: function() {
                  return this.percentText;
                }
              };
            },
            valueAxis: {
              title: {
                text: "millions"
              },
              label: {
                customizeText: function() {
                  return this.value / 1000000 + "M";
                }
              },
              position: "right"
            },
            export: {
              enabled: false
            },
            tooltip: {
              enabled: $tooltipShow,
              location: "edge",
              format: {
                type: $tooltipType,
                precision: $tooltipPrec,
                currency: $tooltipCurr
              },
              customizeTooltip: function(args) {
                return {
                  html:
                    "<div><h5>" +
                    args.argumentText +
                    "</h5></div>" +
                    "<div><b>Amount: </b>" +
                    args.valueText +
                    "</div>" +
                    "<div><b>Percentage: </b>" +
                    (args.percent * 100).toFixed(1) +
                    " %</div>"
                };
              }
            }
          });

          setupPrint(
            $("#locForecastPrint"),
            $("#locForcast"),
            $("#printLocForecastTooltip"),
            "bar"
          );
          setupExport(
            $("#locForecastExport"),
            $("#locForcast"),
            $("#exportLocForecastTooltip")
          );
          setupTableTrigger(
            $("#locForecastTrigger"),
            $("#localForecastTriggerTooltip")
          );

          //Create locForcastTable
          $("#locForcastTable").dxDataGrid({
            dataSource: locForcastTable,
            showBorders: true,
            export: {
              enabled: true,
              fileName: "Local Forcast by Media Type",
              allowExportSelectedData: false
            },
            selection: {
              mode: "single"
            },
            onCellPrepared: function(options) {
              if (options.value) {
                if (options.column.dataField == "cagr") {
                  if (options.value < 0) {
                    options.cellElement.addClass("negative");
                  } else {
                    options.cellElement.addClass("positive");
                  }
                }
              }
            },
            columns: [
              {
                dataField: "name",
                caption: "Media",
                sortOrder: "asc"
              },
              {
                dataField: "yFirst",
                caption: $initYear,
                format: "currency"
              },
              {
                dataField: "yLast",
                caption: $endYear,
                format: "currency"
              },
              {
                dataField: "cagr",
                caption: "CAGR",
                format: {
                  type: "percent",
                  precision: 1
                }
              },
              {
                dataField: "type",
                caption: "Media Type",
                groupIndex: 0
              }
            ],
            sortByGroupSummaryInfo: [
              {
                summaryItem: "count"
              }
            ],
            summary: {
              groupItems: [
                {
                  column: "name",
                  summaryType: "count",
                  displayFormat: "{0}"
                },
                {
                  column: $initYear,
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "Total: {0}",
                  showInGroupFooter: true
                },
                {
                  column: $endYear,
                  summaryType: "sum",
                  valueFormat: "currency",
                  displayFormat: "Total: {0}",
                  showInGroupFooter: true
                }
              ]
            }
          });

          setupPrint($('#locForecastTablePrint'), $('#locForcastTable'), $('#printLocForecastTableTooltip'), 'table');
          //setupExport($('#locForecastTableExport'), $('#locForcastTable'), $('#exportLocForecastTableTooltip'), 'table');
          setupTableTrigger(
            $("#locForecastTableShrink"),
            $("#localForecastTableShrinkTooltip"),
            "",
            "fa fa-compress"
          );
        },
        error: function(err) {
          console.log("Error: Cannot Reach API");
        }
      });

      /*
        // Open Data Table View
        locForcastTableView();
        function locForcastTableView() {
          if ($(".table-button").length < 1) {
            window.requestAnimationFrame(locForcastTableView);
          } else {
            $(".table-button").unbind();
            $(".table-button").on("click", function() {
              $(
                ".card.expand.locForcast ,.locForcast .header.expand ,.locForcast .content.expand"
              ).toggleClass("expanded");
              $(".table-button").toggleClass("clicked");
            });
          }
        }
*/
      // Get Local Digital Ad Spend
      var locdigSpend = [],
        digSpendUrl =
          getBaseHref() +
          "api/MediaAdView/GetCombinedDigitalAdvertising/" +
          $market +
          "/" +
          $initYear;
      $.ajax({
        url: digSpendUrl,
        async: true,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        headers: { "x-api-version": "2.0" },
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Bearer " + $token);
        },
        success: function(locdigSpendData) {
          if (locdigSpendData) {
            var locdigSpendTotal = locdigSpendData.revenue,
              $locdigSpendTotal = $(".locdigSpend-total");
            $locdigSpendTotal.text(
              locdigSpendTotal < 999999999
                ? "$" + (locdigSpendTotal / 1000000).toFixed(0) + " Million"
                : "$" + (locdigSpendTotal / 1000000000).toFixed(1) + " Billion"
            );
            $.each(locdigSpendData.companyDigitalSpendTotals, function(
              index,
              value
            ) {
              locdigSpend.push({
                category: value.name,
                marketTotal: value.value
              });
            });

            $("#locDigSpend").dxPieChart({
              palette: $palletLight,
              dataSource: locdigSpend,
              legend: {
                visible: true,
                rowCount: 3,
                verticalAlignment: "bottom",
                horizontalAlignment: "center",
                itemTextPosition: "right",
                customizeText: function() {
                  var pieChart = $("#locDigSpend").dxPieChart("instance"),
                    percentValue = (
                      pieChart.getAllSeries()[0].getAllPoints()[this.pointIndex]
                        .percent * 100
                    ).toFixed(1);
                  return this.pointName + " (" + percentValue + "%)";
                }
              },
              tooltip: {
                enabled: $tooltipShow,
                format: {
                  type: $tooltipType,
                  precision: $tooltipPrec,
                  currency: $tooltipCurr
                },
                customizeTooltip: function(args) {
                  return {
                    html:
                      "<div><h5>" +
                      args.argumentText +
                      "</h5></div>" +
                      "<div><b>Amount: </b>" +
                      args.valueText +
                      "</div>" +
                      "<div><b>Percentage: </b>" +
                      (args.percent * 100).toFixed(1) +
                      " %</div>"
                  };
                }
              },
              export: {
                enabled: $exportShow
              },
              resolveLabelOverlapping: $labelOverlap,
              adaptiveLayout: {
                width: $adaptLayout
              },
              animation: $animation,
              series: $series,
              onLegendClick: function(info) {
                var pieChart = info.component;
                var argument = info.target;
                pieChart
                  .getAllSeries()[0]
                  .getPointsByArg(argument)[0]
                  .showTooltip();
              }
            });

            $("#locDigSpendCompare").dxPieChart({
              palette: $palletLight,
              dataSource: locdigSpend,
              legend: {
                visible: $legendShow,
                rowCount: 3,
                verticalAlignment: "bottom",
                horizontalAlignment: "center",
                itemTextPosition: "right",
                customizeText: function() {
                  var pieChart = $("#locDigSpendCompare").dxPieChart(
                      "instance"
                    ),
                    percentValue = (
                      pieChart.getAllSeries()[0].getAllPoints()[this.pointIndex]
                        .percent * 100
                    ).toFixed(1);
                  return this.pointName + " (" + percentValue + "%)";
                }
              },
              tooltip: {
                enabled: $tooltipShow,
                format: {
                  type: $tooltipType,
                  precision: $tooltipPrec,
                  currency: $tooltipCurr
                },
                customizeTooltip: function(args) {
                  return {
                    html:
                      "<div><h5>" +
                      args.argumentText +
                      "</h5></div>" +
                      "<div><b>Amount: </b>" +
                      args.valueText +
                      "</div>" +
                      "<div><b>Percentage: </b>" +
                      (args.percent * 100).toFixed(1) +
                      " %</div>"
                  };
                }
              },
              export: {
                enabled: $exportShow
              },
              resolveLabelOverlapping: $labelOverlap,
              adaptiveLayout: {
                width: $adaptLayout
              },
              animation: $animation,
              series: $series,
              onLegendClick: function(info) {
                var pieChart = info.component;
                var argument = info.target;
                pieChart
                  .getAllSeries()[0]
                  .getPointsByArg(argument)[0]
                  .showTooltip();
              }
            });

            setupPrint(
              $("#digitalLocSpendPrint"),
              $("#locDigSpend"),
              $("#printLocDigitalTooltip")
            );
            setupExport(
              $("#digitalLocSpendExport"),
              $("#locDigSpend"),
              $("#exportLocDigitalTooltip")
            );
            setupDigitalSpendTrigger(
              $("#digitalSpendCompareTrigger"),
              $("#digitalCompareTooltip")
            );
          }
        },
        error: function(err) {
          console.log("Error: Cannot Reach API");
        }
      });

      //Update Year Selection
      locDigSpendPrev();
      function locDigSpendPrev() {
        if ($(".locDigSpend .button.prev").length < 1) {
          window.requestAnimationFrame(locDigSpendPrev);
        } else {
          $(".locDigSpend .button.prev").unbind();
          $(".locDigSpend .button.prev").on("click", function() {
            $(".locDigSpend .button.next").removeClass("disabled");
            var $year = $(".locDigSpend .year").text(),
              $yearNum = parseInt($year);
            if ($yearNum != $initYear) {
              $(".locDigSpend .year a").text($yearNum - 1);
            }
            if ($yearNum == $initYear + 1) {
              $(".locDigSpend .button.prev").addClass("disabled");
            }
            var $locDigSpendYr = $(".locDigSpend .year").text(),
              digSpendUrl =
                getBaseHref() +
                "api/MediaAdView/GetCombinedDigitalAdvertising/" +
                $market +
                "/" +
                $locDigSpendYr;
            $.ajax({
              url: digSpendUrl,
              async: true,
              type: "GET",
              dataType: "json",
              contentType: "application/json",
              headers: { "x-api-version": "2.0" },
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $token);
              },
              success: function(locdigSpendData) {
                if (locdigSpendData) {
                  var locdigSpendTotal = locdigSpendData.revenue,
                    $locdigSpendTotal = $(".locdigSpend-total");
                  $locdigSpendTotal.text(
                    locdigSpendTotal < 999999999
                      ? "$" +
                        (locdigSpendTotal / 1000000).toFixed(0) +
                        " Million"
                      : "$" +
                        (locdigSpendTotal / 1000000000).toFixed(1) +
                        " Billion"
                  );
                  locdigSpend = [];
                  $.each(locdigSpendData.companyDigitalSpendTotals, function(
                    index,
                    value
                  ) {
                    locdigSpend.push({
                      category: value.name,
                      marketTotal: value.value
                    });
                  });
                  $("#locDigSpend").dxPieChart(
                    "option",
                    "dataSource",
                    locdigSpend
                  );
                }
              },
              error: function(err) {
                console.log("Error: Cannot Reach API");
              }
            });
          });
        }
      }

      locDigSpendNext();
      function locDigSpendNext() {
        if ($(".locDigSpend .button.next").length < 1) {
          window.requestAnimationFrame(locDigSpendNext);
        } else {
          $(".locDigSpend .button.next").unbind();
          $(".locDigSpend .button.next").on("click", function() {
            $(".locDigSpend .button.prev").removeClass("disabled");
            var $year = $(".locDigSpend .year").text(),
              $yearNum = parseInt($year);
            if ($yearNum != $endYear) {
              $(".locDigSpend .year a").text($yearNum + 1);
            }
            if ($yearNum == $endYear - 1) {
              $(".locDigSpend .button.next").addClass("disabled");
            }
            var $locDigSpendYr = $(".locDigSpend .year").text(),
              digSpendUrl =
                getBaseHref() +
                "api/MediaAdView/GetCombinedDigitalAdvertising/" +
                $market +
                "/" +
                $locDigSpendYr;
            $.ajax({
              url: digSpendUrl,
              async: true,
              type: "GET",
              dataType: "json",
              contentType: "application/json",
              headers: { "x-api-version": "2.0" },
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $token);
              },
              success: function(locdigSpendData) {
                if (locdigSpendData) {
                  var locdigSpendTotal = locdigSpendData.revenue,
                    $locdigSpendTotal = $(".locdigSpend-total");
                  $locdigSpendTotal.text(
                    locdigSpendTotal < 999999999
                      ? "$" +
                        (locdigSpendTotal / 1000000).toFixed(0) +
                        " Million"
                      : "$" +
                        (locdigSpendTotal / 1000000000).toFixed(1) +
                        " Billion"
                  );
                  locdigSpend = [];
                  $.each(locdigSpendData.companyDigitalSpendTotals, function(
                    index,
                    value
                  ) {
                    locdigSpend.push({
                      category: value.name,
                      marketTotal: value.value
                    });
                  });
                  $("#locDigSpend").dxPieChart(
                    "option",
                    "dataSource",
                    locdigSpend
                  );
                }
              },
              error: function(err) {
                console.log("Error: Cannot Reach API");
              }
            });
          });
        }
      }

      //Update Local Digital Spend Compare Year Selection
      locDigSpendComparePrev();
      function locDigSpendComparePrev() {
        if ($(".locDigSpendCompare .button.prev").length < 1) {
          window.requestAnimationFrame(locDigSpendComparePrev);
        } else {
          $(".locDigSpendCompare .button.prev").unbind();
          $(".locDigSpendCompare .button.prev").on("click", function() {
            $(".locDigSpendCompare .button.next").removeClass("disabled");
            var $year = $(".locDigSpendCompare .year").text(),
              $yearNum = parseInt($year);
            if ($yearNum != $initYear) {
              $(".locDigSpendCompare .year a").text($yearNum - 1);
            }
            if ($yearNum == $initYear + 1) {
              $(".locDigSpendCompare .button.prev").addClass("disabled");
            }
            var $locDigSpendYr = $(".locDigSpendCompare .year").text(),
              digSpendUrl =
                getBaseHref() +
                "api/MediaAdView/GetCombinedDigitalAdvertising/" +
                $market +
                "/" +
                $locDigSpendYr;
            $.ajax({
              url: digSpendUrl,
              async: true,
              type: "GET",
              dataType: "json",
              contentType: "application/json",
              headers: { "x-api-version": "2.0" },
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $token);
              },
              success: function(locdigSpendData) {
                if (locdigSpendData) {
                  var locdigSpendTotal = locdigSpendData.revenue,
                    $locdigSpendTotal = $(".locdigSpend-total");
                  $locdigSpendTotal.text(
                    locdigSpendTotal < 999999999
                      ? "$" +
                        (locdigSpendTotal / 1000000).toFixed(0) +
                        " Million"
                      : "$" +
                        (locdigSpendTotal / 1000000000).toFixed(1) +
                        " Billion"
                  );
                  locdigSpend = [];
                  $.each(locdigSpendData.companyDigitalSpendTotals, function(
                    index,
                    value
                  ) {
                    locdigSpend.push({
                      category: value.name,
                      marketTotal: value.value
                    });
                  });
                  $("#locDigSpendCompare").dxPieChart(
                    "option",
                    "dataSource",
                    locdigSpend
                  );
                }
              },
              error: function(err) {
                console.log("Error: Cannot Reach API");
              }
            });
          });
        }
      }

      locDigSpendCompareNext();
      function locDigSpendCompareNext() {
        if ($(".locDigSpendCompare .button.next").length < 1) {
          window.requestAnimationFrame(locDigSpendCompareNext);
        } else {
          $(".locDigSpendCompare .button.next").unbind();
          $(".locDigSpendCompare .button.next").on("click", function() {
            $(".locDigSpendCompare .button.prev").removeClass("disabled");
            var $year = $(".locDigSpendCompare .year").text(),
              $yearNum = parseInt($year);
            if ($yearNum != $endYear) {
              $(".locDigSpendCompare .year a").text($yearNum + 1);
            }
            if ($yearNum == $endYear - 1) {
              $(".locDigSpendCompare .button.next").addClass("disabled");
            }
            var $locDigSpendYr = $(".locDigSpendCompare .year").text(),
              digSpendUrl =
                getBaseHref() +
                "api/MediaAdView/GetCombinedDigitalAdvertising/" +
                $market +
                "/" +
                $locDigSpendYr;
            $.ajax({
              url: digSpendUrl,
              async: true,
              type: "GET",
              dataType: "json",
              contentType: "application/json",
              headers: { "x-api-version": "2.0" },
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $token);
              },
              success: function(locdigSpendData) {
                if (locdigSpendData) {
                  var locdigSpendTotal = locdigSpendData.revenue,
                    $locdigSpendTotal = $(".locdigSpend-total");
                  $locdigSpendTotal.text(
                    locdigSpendTotal < 999999999
                      ? "$" +
                        (locdigSpendTotal / 1000000).toFixed(0) +
                        " Million"
                      : "$" +
                        (locdigSpendTotal / 1000000000).toFixed(1) +
                        " Billion"
                  );
                  locdigSpend = [];
                  $.each(locdigSpendData.companyDigitalSpendTotals, function(
                    index,
                    value
                  ) {
                    locdigSpend.push({
                      category: value.name,
                      marketTotal: value.value
                    });
                  });
                  $("#locDigSpendCompare").dxPieChart(
                    "option",
                    "dataSource",
                    locdigSpend
                  );
                }
              },
              error: function(err) {
                console.log("Error: Cannot Reach API");
              }
            });
          });
        }
      }

      var natdigSpend = [],
        natdigSpendUrl =
          getBaseHref() +
          "api/MediaAdView/GetNationwideCombinedDigitalAdvertising/" +
          $initYear;
      $.ajax({
        url: natdigSpendUrl,
        async: true,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        headers: { "x-api-version": "2.0" },
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Bearer " + $token);
        },
        success: function(natdigSpendData) {
          if (natdigSpendData) {
            var natdigSpendTotal = natdigSpendData.revenue,
              $natdigSpendTotal = $(".natdigSpend-total");
            $natdigSpendTotal.text(
              natdigSpendTotal < 999999999
                ? "$" + (natdigSpendTotal / 1000000).toFixed(0) + " Million"
                : "$" + (natdigSpendTotal / 1000000000).toFixed(1) + " Billion"
            );
            $.each(natdigSpendData.companyDigitalSpendTotals, function(
              index,
              value
            ) {
              natdigSpend.push({
                category: value.name,
                marketTotal: value.value
              });
            });

            $("#natDigSpend").dxPieChart({
              palette: $palletLight,
              dataSource: natdigSpend,
              legend: {
                visible: $legendShow,
                rowCount: 3,
                verticalAlignment: "bottom",
                horizontalAlignment: "center",
                itemTextPosition: "right",
                customizeText: function() {
                  var pieChart = $("#natDigSpend").dxPieChart("instance"),
                    percentValue = (
                      pieChart.getAllSeries()[0].getAllPoints()[this.pointIndex]
                        .percent * 100
                    ).toFixed(1);
                  return this.pointName + " (" + percentValue + "%)";
                }
              },
              tooltip: {
                enabled: $tooltipShow,
                format: {
                  type: $tooltipType,
                  precision: $tooltipPrec,
                  currency: $tooltipCurr
                },
                customizeTooltip: function(args) {
                  return {
                    html:
                      "<div><h5>" +
                      args.argumentText +
                      "</h5></div>" +
                      "<div><b>Amount: </b>" +
                      args.valueText +
                      "</div>" +
                      "<div><b>Percentage: </b>" +
                      (args.percent * 100).toFixed(1) +
                      " %</div>"
                  };
                }
              },
              export: {
                enabled: $exportShow
              },
              resolveLabelOverlapping: $labelOverlap,
              adaptiveLayout: {
                width: $adaptLayout
              },
              animation: $animation,
              series: $series,
              onLegendClick: function(info) {
                var pieChart = info.component;
                var argument = info.target;
                pieChart
                  .getAllSeries()[0]
                  .getPointsByArg(argument)[0]
                  .showTooltip();
              }
            });

            setupComparePrint(
              $("#digitalLocNatSpendPrint"),
              $("#natDigSpend"),
              $("#locDigSpendCompare"),
              $("#printLocNatDigitalTooltip")
            );
            setupCompareExport(
              $("#digitalLocNatSpendExport"),
              $("#natDigSpend"),
              $("#locDigSpendCompare"),
              $("#exportLocNatDigitalTooltip")
            );
            setupDigitalSpendTrigger(
              $("#digitalSpendCompareShrinkTrigger"),
              $("#digitalCompareShrinkTooltip"),
              "Close",
              "fa fa-compress"
            );
          }
        },
        error: function(err) {
          console.log("Error: Cannot Reach API");
        }
      });

      //Update Year Selection
      natDigSpendPrev();
      function natDigSpendPrev() {
        if ($(".natDigSpend .button.prev").length < 1) {
          window.requestAnimationFrame(natDigSpendPrev);
        } else {
          $(".natDigSpend .button.prev").unbind();
          $(".natDigSpend .button.prev").on("click", function() {
            $(".natDigSpend .button.next").removeClass("disabled");
            var $year = $(".natDigSpend .year").text(),
              $yearNum = parseInt($year);
            if ($yearNum != $initYear) {
              $(".natDigSpend .year a").text($yearNum - 1);
            }
            if ($yearNum == $initYear + 1) {
              $(".natDigSpend .button.prev").addClass("disabled");
            }
            var $natDigSpendYr = $(".natDigSpend .year").text(),
              digSpendUrl =
                getBaseHref() +
                "api/MediaAdView/GetNationwideCombinedDigitalAdvertising/" +
                $natDigSpendYr;
            $.ajax({
              url: digSpendUrl,
              async: true,
              type: "GET",
              dataType: "json",
              contentType: "application/json",
              headers: { "x-api-version": "2.0" },
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $token);
              },
              success: function(natdigSpendData) {
                if (natdigSpendData) {
                  var natdigSpendTotal = natdigSpendData.revenue,
                    $natdigSpendTotal = $(".natdigSpend-total");
                  $natdigSpendTotal.text(
                    natdigSpendTotal < 999999999
                      ? "$" +
                        (natdigSpendTotal / 1000000).toFixed(0) +
                        " Million"
                      : "$" +
                        (natdigSpendTotal / 1000000000).toFixed(1) +
                        " Billion"
                  );
                  natdigSpend = [];
                  $.each(natdigSpendData.companyDigitalSpendTotals, function(
                    index,
                    value
                  ) {
                    natdigSpend.push({
                      category: value.name,
                      marketTotal: value.value
                    });
                  });
                  $("#natDigSpend").dxPieChart(
                    "option",
                    "dataSource",
                    natdigSpend
                  );
                }
              },
              error: function(err) {
                console.log("Error: Cannot Reach API");
              }
            });
          });
        }
      }

      natDigSpendNext();
      function natDigSpendNext() {
        if ($(".natDigSpend .button.next").length < 1) {
          window.requestAnimationFrame(natDigSpendNext);
        } else {
          $(".natDigSpend .button.next").unbind();
          $(".natDigSpend .button.next").on("click", function() {
            $(".natDigSpend .button.prev").removeClass("disabled");
            var $year = $(".natDigSpend .year").text(),
              $yearNum = parseInt($year);
            if ($yearNum != $endYear) {
              $(".natDigSpend .year a").text($yearNum + 1);
            }
            if ($yearNum == $endYear - 1) {
              $(".natDigSpend .button.next").addClass("disabled");
            }
            var $natDigSpendYr = $(".natDigSpend .year").text(),
              digSpendUrl =
                getBaseHref() +
                "api/MediaAdView/GetNationwideCombinedDigitalAdvertising/" +
                $natDigSpendYr;
            $.ajax({
              url: digSpendUrl,
              async: true,
              type: "GET",
              dataType: "json",
              contentType: "application/json",
              headers: { "x-api-version": "2.0" },
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $token);
              },
              success: function(natdigSpendData) {
                if (natdigSpendData) {
                  var natdigSpendTotal = natdigSpendData.revenue,
                    $natdigSpendTotal = $(".digSpend-total");
                  $natdigSpendTotal.text(
                    natdigSpendTotal < 999999999
                      ? "$" +
                        (natdigSpendTotal / 1000000).toFixed(0) +
                        " Million"
                      : "$" +
                        (natdigSpendTotal / 1000000000).toFixed(1) +
                        " Billion"
                  );
                  natdigSpend = [];
                  $.each(natdigSpendData.companyDigitalSpendTotals, function(
                    index,
                    value
                  ) {
                    natdigSpend.push({
                      category: value.name,
                      marketTotal: value.value
                    });
                  });
                  $("#natDigSpend").dxPieChart(
                    "option",
                    "dataSource",
                    natdigSpend
                  );
                }
              },
              error: function(err) {
                console.log("Error: Cannot Reach API");
              }
            });
          });
        }
      }
      /*
        // Open Digital Spend Compare
        digSpendCompare();
        function digSpendCompare() {
          if ($(".digSpendCompareTrigger").length < 1) {
            window.requestAnimationFrame(digSpendCompare);
          } else {
            $(".digSpendCompareTrigger").unbind();
            $(".digSpendCompareTrigger").on("click", function() {
              $(
                ".card.expand.digSpendCompare ,.digSpendCompare .header.expand ,.digSpendCompare .content.expand"
              ).toggleClass("expanded");
              $(".table-button").toggleClass("disabled");
            });
          }
        }
*/
      //Get Vertical Ad Summary
      var vertUrl =
          getBaseHref() +
          "api/MediaAdView/GetTopVerticalsByMedia/" +
          $market +
          "/" +
          ($initYear - 1) +
          "/" +
          $initYear,
        vert = [];
      $.ajax({
        url: vertUrl,
        async: true,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        headers: { "x-api-version": "2.0" },
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Bearer " + $token);
        },
        success: function(vertdata) {
          if (vertdata) {
            $.each(vertdata, function(index, value) {
              if (value.year == $initYear) {
                $.each(value.verticalEstimates, function(index1, value1) {
                  vert.push({
                    name: value1.name,
                    pTotal: 0,
                    cTotal: value1.total,
                    percent: value1.mediaPercentTotal,
                    change: value1.percentageChange
                  });
                });
              }
            });
            var i = 0;
            $.each(vertdata, function(index, value) {
              if (value.year == $initYear - 1) {
                $.each(value.verticalEstimates, function(index2, value2) {
                  vert[i].pTotal = value2.total;
                  i++;
                });
              }
            });
            $("#vertSum").dxDataGrid({
              dataSource: vert,
              export: {
                enabled: true,
                fileName: $initYear + " Vertical Ad Spending",
                allowExportSelectedData: false
              },
              onCellPrepared: function(options) {
                if (options.value) {
                  if (options.column.dataField == "change") {
                    if (options.value < 0) {
                      options.cellElement.addClass("negative");
                    } else {
                      options.cellElement.addClass("positive");
                    }
                  }
                }
              },
              summary: {
                totalItems: [
                  {
                    column: "pTotal",
                    summaryType: "sum",
                    customizeText: function(data) {
                      return (
                        "$" +
                        data.value
                          .toFixed(0)
                          .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
                      );
                    }
                  },
                  {
                    column: "cTotal",
                    summaryType: "sum",
                    customizeText: function(data) {
                      return (
                        "$" +
                        data.value
                          .toFixed(0)
                          .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
                      );
                    }
                  }
                ]
              },
              columns: [
                {
                  dataField: "name",
                  caption: "Vertical",
                  sortOrder: "asc"
                },
                {
                  dataField: "pTotal",
                  caption: $initYear - 1 + " Local Advertising",
                  format: "currency"
                },
                {
                  dataField: "cTotal",
                  caption: $initYear + " Local Advertising",
                  format: "currency"
                },
                {
                  dataField: "percent",
                  caption: "Percent of Total",
                  format: {
                    type: "percent",
                    precision: 1
                  }
                },
                {
                  dataField: "change",
                  caption: "Annual Change",
                  format: {
                    type: "percent",
                    precision: 1
                  }
                }
              ]
            });
            
             setupPrint(
              $("#vertAdSumPrint"),
              $("#vertSum"),
              $("#printVertAdSumTooltip"),
               "table"
            );
            
          }
        },
        error: function(err) {
          console.log("Error: Cannot Reach API");
        }
      });

      //Update Year Selection
      //vertPrev();
      function vertPrev() {
        if ($(".vert .button.prev").length < 1) {
          window.requestAnimationFrame(vertPrev);
        } else {
          $(".vert .button.prev").unbind();
          $(".vert .button.prev").on("click", function() {
            $(".vert .button.next").removeClass("disabled");
            var $year = $(".vert .year").text(),
              $yearNum = parseInt($year);
            if ($yearNum != $initYear) {
              $(".vert .year a").text($yearNum - 1);
            }
            if ($yearNum == parseInt($initYear) + 1) {
              $(".vert .button.prev").addClass("disabled");
            }
            var $vertYr = $(".vert .year").text(),
              vertUrl =
                getBaseHref() +
                "api/MediaAdView/GetTopVerticalsByMedia/" +
                $market +
                "/" +
                ($vertYr - 1) +
                "/" +
                $vertYr,
              vertPrev = [];
            $.ajax({
              url: vertUrl,
              async: true,
              type: "GET",
              dataType: "json",
              contentType: "application/json",
              headers: { "x-api-version": "2.0" },
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $token);
              },
              success: function(vertData) {
                if (vertData) {
                  $.each(vertData, function(index, value) {
                    if (value.year == $vertYr) {
                      $.each(value.verticalEstimates, function(index1, value1) {
                        vertPrev.push({
                          name: value1.name,
                          pTotal: 0,
                          cTotal: value1.total,
                          percent: value1.mediaPercentTotal,
                          change: value1.percentageChange
                        });
                      });
                    }
                  });
                  var i = 0;
                  $.each(vertData, function(index, value) {
                    if (value.year == $vertYr - 1) {
                      $.each(value.verticalEstimates, function(index2, value2) {
                        vertPrev[i].pTotal = value2.total;
                        i++;
                      });
                    }
                  });
                  $("#vertSum").dxDataGrid("option", "dataSource", vertPrev);
                  var $vertSum = $("#vertSum").dxDataGrid("instance");
                  $vertSum.columnOption(
                    "cTotal",
                    "caption",
                    $yearNum - 1 + " Local Advertising"
                  );
                  $vertSum.columnOption(
                    "pTotal",
                    "caption",
                    $yearNum - 2 + " Local Advertising"
                  );
                }
              },
              error: function(err) {
                console.log("Error: Cannot Reach API");
              }
            });
          });
        }
      }

      //vertNext();
      function vertNext() {
        if ($(".vert .button.next").length < 1) {
          window.requestAnimationFrame(vertNext);
        } else {
          $(".vert .button.next").unbind();
          $(".vert .button.next").on("click", function() {
            $(".vert .button.prev").removeClass("disabled");
            var $year = $(".vert .year").text(),
              $yearNum = parseInt($year);
            if ($yearNum != $endYear) {
              $(".vert .year a").text($yearNum + 1);
            }
            if ($yearNum == $endYear - 1) {
              $(".vert .button.next").addClass("disabled");
            }
            var $vertYr = $(".vert .year").text(),
              vertUrl =
                getBaseHref() +
                "api/MediaAdView/GetTopVerticalsByMedia/" +
                $market +
                "/" +
                ($vertYr - 1) +
                "/" +
                $vertYr,
              vertNext = [];
            $.ajax({
              url: vertUrl,
              async: true,
              type: "GET",
              dataType: "json",
              contentType: "application/json",
              headers: { "x-api-version": "2.0" },
              beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $token);
              },
              success: function(vertData) {
                if (vertData) {
                  $.each(vertData, function(index, value) {
                    if (value.year == $vertYr) {
                      $.each(value.verticalEstimates, function(index1, value1) {
                        vertNext.push({
                          name: value1.name,
                          pTotal: 0,
                          cTotal: value1.total,
                          percent: value1.mediaPercentTotal,
                          change: value1.percentageChange
                        });
                      });
                    }
                  });
                  var i = 0;
                  $.each(vertData, function(index, value) {
                    if (value.year == $vertYr - 1) {
                      $.each(value.verticalEstimates, function(index2, value2) {
                        vertNext[i].pTotal = value2.total;
                        i++;
                      });
                    }
                  });
                  $("#vertSum").dxDataGrid("option", "dataSource", vertNext);
                  var $vertSum = $("#vertSum").dxDataGrid("instance");
                  $vertSum.columnOption(
                    "cTotal",
                    "caption",
                    $yearNum + 1 + " Local Advertising"
                  );
                  $vertSum.columnOption(
                    "pTotal",
                    "caption",
                    $yearNum + " Local Advertising"
                  );
                }
              },
              error: function(err) {
                console.log("Error: Cannot Reach API");
              }
            });
          });
        }
      }
    }
  }
});
